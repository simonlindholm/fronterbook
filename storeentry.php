<?php
$text = $_GET['text'];
$ownerid = $_GET['user'];
// Type 1 for entries. Note: bind_param takes references, so we can't use
// a literal instead.
$type = 1;

// Make sure the comment is <= 1000 characters.
if(strlen($text) > 1000)
{
	echo('throw new Error("Entry too long");');
	exit();
}

// Store it in the database.
$mysqli = new mysqli('localhost', 'root', 'password', 'fronterbook') or die('throw new Error("Could not connect to database");'); 
if($statement = $mysqli->prepare('INSERT INTO fronterbook_event (ownerid, type, text) VALUES (?, ?, ?)')) {
	$statement->bind_param('iis', $ownerid, $type, $text);
	$statement->execute();
	$statement->close();
}
else {
	echo 'throw new Error("Could not put post in table.");';
}
$mysqli->close();
 
// If everything works out, give a periodic update back.
include('getevents.php');
?>
