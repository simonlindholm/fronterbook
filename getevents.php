<?php

if (!isset($_GET['after']) || !ctype_digit($_GET['after'])) {
	die('throw new Error("Limit required.");');
}
$after = $_GET['after']-0;

$time = time();

$mysqli = new mysqli('localhost', 'root', 'password', 'fronterbook') or die('throw new Error("Could not connect to database");');

// Get events and pass them to callback as an array
$q = 'SELECT id, ownerid, type, text, pointerid, UNIX_TIMESTAMP(timestamp) AS time FROM fronterbook_event';
if ($after !== 0) {
	$q .= ' WHERE id > ' . $after;
}
$q .= ' ORDER BY id';

if ($result = $mysqli->query($q)) {
	$out = '[';
	$first = true;
	$nextId = $after+1;
	while ($row = $result->fetch_object())
	{
		if ($first) $first = false;
		else $out .= ',';

		// Fill out deleted events as null (so that the client can still keep
		// up with id's).
		$id = $row->id-0;
		while ($nextId < $id) {
			$out .= 'null,';
			++$nextId;
		}
		++$nextId;

		$type = $row->type-0;

		$out .= "[$type,{$row->ownerid},";
		if ($type === 0 || $type === 2) { // Comment or like
			$out .= $row->pointerid . ',';
		}
		if ($type === 0 || $type === 1) { // Comment or entry
			$out .= json_encode($row->text) . ',';
		}
		$out .= $row->time . "]\n";
	}
	$out .= ']';

	$result->close();

	$mysqli->close();

	// Output everything.
	echo "callbackGetEvents($after,$time,\n", $out;
	if (isset($_GET['o'])) {
		include('online.php');
	}
	echo ');';
}
else {
	$mysqli->close();
}
?>
