<?php
$oinfo = explode(',', $_GET['o']);
$u = $oinfo[0]-0;
$after = $oinfo[1]-0;

// Respond with what has happened to online statuses lately.

$mysqli = new mysqli('localhost', 'root', 'password', 'fronterbook') or die('throw new Error("Could not connect to database");');

$time = time();
$old = $time - 13;

// Mark user $u as online.
if ($result = $mysqli->query('SELECT id, online FROM fronterbook_online WHERE user = ' . $u)) {
	$row = $result->fetch_object();
	$alreadyOnline = false;
	if ($row) {
		if ($row->online) {
			$alreadyOnline = true;
			$q = 'UPDATE fronterbook_online SET last_access = NOW()';
		}
		else {
			$q = 'DELETE FROM fronterbook_online';
		}
		$q .= ' WHERE user = ?';
		if ($s = $mysqli->prepare($q)) {
			$s->bind_param('i', $u);
			$s->execute();
			$s->close();
		}
	}
	if (!$alreadyOnline) {
		if ($s = $mysqli->prepare('INSERT INTO fronterbook_online (user, online) VALUES (?, 1)')) {
			$s->bind_param('i', $u);
			$s->execute();
			$s->close();
		}
	}
	$result->close();
}

// Mark non-responsive users as offline.
$where = "WHERE online = 1 AND UNIX_TIMESTAMP(last_access) < $old";
if ($result = $mysqli->query("SELECT user FROM fronterbook_online $where")) {
	if ($s = $mysqli->prepare("DELETE FROM fronterbook_online $where")) {
		$s->execute();
		$s->close();
	}
	while ($row = $result->fetch_object()) {
		if ($s = $mysqli->prepare('INSERT INTO fronterbook_online (user, online) VALUES (?, 0)')) {
			$s->bind_param('i', $row->user);
			$s->execute();
			$s->close();
		}
	}
	$result->close();
}
else {
	echo $mysqli->error;
}

// Send back rows with id > $after.
if ($result = $mysqli->query("SELECT id, user, online FROM fronterbook_online WHERE id > $after")) {
	$first = true;
	$max = $after;
	while ($row = $result->fetch_object()) {
		$id = $row->id;
		$user = $row->user;
		$online = $row->online;
		if ($user == $u) continue;
		if ($first) {
			$first = false;
			echo ',[';
		}
		else echo ',';
		if ($id > $max) $max = $id;
		echo "[$user,$online]";
	}
	$result->close();
	if (!$first) echo "],$max";
}
$mysqli->close();
?>
