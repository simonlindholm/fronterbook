<?php
$pointerid = $_GET['id'];
$ownerid = $_GET['user'];

// Type 2 for likes. Note: bind_param takes references, so we can't use
// a literal instead.
$type = 2;

// Store it in the database.
$mysqli = new mysqli('localhost', 'root', 'password', 'fronterbook') or die('throw new Error("Could not connect to database");'); 
if($statement = $mysqli->prepare('INSERT INTO fronterbook_event (ownerid, type, pointerid) VALUES (?, ?, ?)')) {
	$statement->bind_param('iii', $ownerid, $type, $pointerid);
	$statement->execute();
	$statement->close();
}
else {
	echo 'throw new Error("Could not put post in table.");';
}
$mysqli->close();
 
// If everything works out, give a periodic update back.
include('getevents.php');
?>
