(function() {
"use strict";

// Don't load the top script twice.
if (window.scLoaded) return;
window.scLoaded = true;

// User info (7kB).
var classData = {
"SP3A":"09viki21rd2i360v8t3ci7963n5to23npadi41doru4tagq258hmkg7t406g8028uk81a02sbj6oaccohmd9dqa6jjhnkahoif7204ipma61j4jj9hkkajllksstjbld2ue7mcvrv4ot9jl7perovgqa47s0qm30egrni662rub2r8sp3ac6t0grmutj5s64",
"NA1D":"01u7670ol7pv0sfbgd1bdk1t1r5efq3h9ea83vabep45hc1f5adugg5il7qa60r3ha75ifdfbl9vb8fol604gmjs77h8grn9hfmuscibtijuk165lik9psbvlgn8rpmf0tfpn709g2nov6hnojcvldpgd10epnk6esq81hs3s6ass0u0r95ju5bvhaugi161ut6dj8",
"SPR1C":"12uon02k7g6l5uvelva2hdmbbjeamkhqe7vdi7h894ihd7jvlanv4nm6nfirpkqelvsgn72dtef9dnu7i3pb",
"NV3D":"0emf5c2crf992hb4ca3ubbt04bp7qt63qp8h6ed7do6fbtpd6iauka7aeekf7v3eed8jdh829m4s7pa3509aacodqkapt9fkcbhdd6cg795re111dff9umgbief15iljr3cjlvra7sm037mpm9lcvupt90g6qhb4uhu2muv2uoemgqvgj8cv",
"EK1C":"08fiu20ko25m0ulmm32h4n272ikoqi322i3g3cq5oo52bkh485a9lo8nfldi8sbgd09gmrtka1o6g4a6p599ahksj4apsi9beeqtn5fpqmkkg1mh8qj7smdpjel0svk1jib8m7nbkaoe83qaokrpcbopqgdop67hfqrg2lfdrpq5ousap0bhv2anmtvgb43fvuoo8o",
"NV3B":"1cvij239go4c46nek951cbjn5f3gh07cbp449g45olaeq8fqb3gu6ebfum18ctgj71dvdctke4k1uhe6ir5keffkn8h27jd2jioaf6kpkaqdl3sam9ltucabml2s8kmuru8knbtqq7nvv2srp7run3qatm4or17cjgrl7pa3tkeq64uo2p81",
"ES1":"1o3lg3239i5f3cvoqa55ktbg57h5l98hgcut9vbogaacr8drd63vjbfus24vgej538getq3iho1kpri6ebfvj57a77jc2itejo6635k5mu3lmetohto967ueqbcav5qs8uf3qv0f1ms7daqvss3glnt4tvq7uo5u98v2lg98vhh5o9",
"NA1C":"1m3dhh346toh3gcgvd44scv1502ps968915v8bkoa69705cpb1jntsc274kickidpcdn32dedrh507eioajnfjlqgfgfve33h3e8gbhsfsjsidpik0iuh7ncov7i97pievp3qg35tjqtd5hjr4pl9vsnujr7some79t4vbvctas3j2truahkud8atoviu9as",
"SP3C":"0pihf41nilaq473fab4ak3oj4b8rtu4bunqk5fplit5icr035ta3v979nijh84qema9breb0a19g9kaetkatb5hnijbj418nd0fb5edon7hgfk9nvbg5hrrelad1ormp5ohimuo4jnooeh31pil92nqou0kjs13loesqhogrvijuoevotuac",
"NASA1B":"07a4390al2ei0r0ubo16ptbv1pt62r1qa2432rdmu736pteo3soong5cpusi65ss2d8pjb409jtbufa4et8aajmcdob4ni16b6ve45b78lkkd0u77kf3d4tlf3td4lgcpjtkgijcaljdgk75ptnaocqdvl4stmjb24ttbtp4veqollvjhqtvvlj596",
"SA1B":"0m7n9h1ctboa2nssr53eq5nv6h7onl8qk0gja41u2hasrt44csku0qefu9aqfv72k8gndsnkh8fp91hekfdihm3m5iiah5tfjdp265jj7vu0l02dn0l0hbfml9hrfvlhfo14llcbdalnrhdcmrvbbjndcdbootfin3rg2oent289psudiablus23aevjf0qa",
"EK1B":"076on818kjju2s7gdq47auqr4pq38958bifp5dp8ve6vhm5j99c3cl99j62k9u9btoa14mgcb7fd8icmfun8daah9udt4oipg8n9uiga3q31h6qq7vjdpb23jvqbr7kf53vrkp4rsvm9gj4imvhplvpp3ps2q46o15q61ncnr34dpsrivo8csfh1p3sqg27s",
"TE3":"7lpjb18t3n9c9ioj8oaiqq31anaripbd9v8fbs149tcvmr3geb8bb2gnm84rhae4quhukt6iiclb6kimmcvtinb2d3j0rf68jtngerkr6c12m1rh8vm28mione0lhfodghpgp1d86crt8157t18vp2tl5pn8u2qk2ju9hoibub6ne2ujljbtunuig3",
"NV2B":"0h5qi60st1hh1gpr5i1tue5s1uij802ek2sg2penka313942472rt34vqjuu5a6ase98grs49c76f09page5br68hlfq7bhehc42q4hn4ev3i3pdfpj95vrskt3t87qi710mrilos8s4dlm6um1gtevale2s",
"NV2A":"3o5shp5n32bo5q7cq76a663a947ubjg05gpqg8h780grs2c0j2m8f6j6cqeujej7f8ji07s1jjrrsljp9skekas2lkkmbh6hkmdqh0l4s580l6lqikm8v03rpckbjur0930er2v62st6k95hual0gbv8kj9o",
"NA1B":"0e7hev13vrrq1s2id71tk3r12bgtkq35nu3u3g2dau3gklae3kli976kahqg8fjp419ujj0ta5jpugahbvlibo3jfac9eejucp44hkefk1n5eki1maen9du6hlds89ikk67vitd0c5kes8r5lbifgklssmiqmt767dn8ft2in9d5aanerklnphnba4udruh9",
"NV3C":"0cc85h142tig1tqb1c3lcac652iih168seai6hplbh6t4mqe860bfk8eari0bbei8pbe10add42m0vdllgt0e8b2jvgf8po7gs9gouhqqas0igqugfjkve8ble4hn5lk0odtmlq6aqn67sjso2avs1o3s697pl529vq7adpkvsd1v3vsvhvq",
"EK1A":"29haln6ifo2p6lvn0079iora7a5tju8boembaj2qngbag18qbf1f8bc4iik5c9kss0ddiikke9l2hqf4cdl1gl82s3gn9k4fhj5mi3j9qisvkk0j61l7c1uflo1vpdm6acqsnv0hvhqagaadr99u28sp35phss8224tbge0tud55fsuge1sl",
"SP2A":"07vrgh0an2aq0h4vi20s1rco0tfq352t339c4qr8on4rmvpi4t2go45daqn877te5j7saiop932u3oausq3lbvoqtddkei34f5jo9cfd9c5pg9gn21h4a856h7hmqkhjo0khibt8fgiiodasj5fsfvk0db6anbfka7pe6gl6rsvh7atdt421vdu70ovk1sff",
"NASA1A":"0f89l60rjdp51k0u0t23bgf823l32p3dl33o4vp29q5a38kk6jmqd86krds877hpvf8ljgst8qve9o8s2c9e91l59e9lc544amu76ab80jdae2sglnevhjlah51o94hn6ottiiosr7jmvv3hknajp8ksohr3l0759jll12mcmiv8bftjfc4otqrjqv",
"SM2B":"05ud5a1ldins2apscm2cbvdv388j363cjqko6i392b7h5sf280ta4ra5toruasn6l5co74vof4jbb2f8ai38g5gt3jgjrmj1hut0hfj5p4n9jj9jg3kjk4brl1elbtmos400o11oe9pfpvd1tge7uluuh39jv3p830v3spt1v64oe1v9ltvr",
"SPR1B":"0egv140ub39916v7v026s29q43uqs1aeu5v8dh3f4udmg39ceb541rg4q3abibcrkvpul8fvqils5nrl86ur",
"SP2C":"0mvnsq1j4i5q2f5vmj31iin448cj8n80uchq9me5nnbh4ce9d3otrud5aj4hf899gaffhpfaflrg48gdhaa3gn430mhekopthhvku1hs06k8hss4j2kan4tqkcj72vktvk4pna1t8hnjkqjcnqsucpoknqc2p2qvoep3g2pnqiqmn4rh3eiisb6ke8sd36qh",
"NV2C":"05luvj1lqjre2qt3hv56er7l6dhel39cvspp9hkme4apba8uapdqkffipl08fm459pgiu53aj8elppl0fuiulc3i78lcfdb7mpamnsndqsbqnhil09p319cgqkb5csqlvt95rd19osrj70mhrnjdjqrvuuvqtsvbc7ulg0ev",
"SM2C":"0d1j230gn9dn3cjeac3hqgav3r7coe45m0ll4uc71d5g67atbkbroec03t6bckvoandkul1mduup2rf1fkt2f6djcjfdh50mflqei6hd89mhjpm86bk9fg05kiqg7ul1914flba8v5mu6c8on6jp6loaodkep3uknlp5q5dvpv5m91q9kcn2r8vpr4rjpe30",
"NV2D":"0m5t5d182ehg1ihmsa1kp7i362ilrv65omar67lcth6okb7074rs3a78nj1o92rlfeb5be28bbupvkgppjl0gu8nalh118duh8b9fgidkf9albj3d0ll9gq7mej9mko93bdgpph96nptijksr0k245tcp7ulttip9cutlnk1v8e7obvcot37",
"SP2B":"0c44f60pb6fd1mj1m52ines1385va13t4k5k3utodj4g8ivf4gpsjm61fdre6ut5iba1h04tabck3pbitf4bc1mv77d4hgkvfrgad8g0rnnagumjl0hg8s3fi2khtlilsd5ak1s11jkavassofqgq6qtb10js9r357t4v5c6u8325durat8vvf2g7e",
"NV3A":"0ghuim0s4bdt3le7fo3vf83540krda6ln5d98ui0ko8vj71qadq8ovbmbdd8c9640mdmlelue9ra1ef6eivjfjgf2pfl3kllflr0s1fr3uiqh8t6a2hlt94ii5f31cjjn29akooa9emltte8oj4vpcqj1musqspso7r1jdcot6uis0u0vc11vjj6c7",
"NA1A":"00vddl0fc4av1o73r968l0ct6n5jtj87cve7a6cqaubeqipkc0sdi6d0u61idipghfeip85eeugn3aioeqqsj33139j6n5ogjfperdkiad54kjus6al1trtdlht80qn5nbqeno87upoe4rdtpn7ftnr618lerlehgltneheq",
"SM3A":"0bpkrn0lhqe71tt0us2c36l23kf1b16mlcu56rbh4p8bpqhi8hr7q9933re6dpbp5kicdch4ivrcgtk77dculnel3dm9epaen7v2rsnp0c2knpal98qr2dmequj7css66rb1t6nv6vtqci2cub9tlguivb7mv66lvs",
"SM2A":"1ad59e1jt68c27jib754n41b5a777d6bk5sh77jk16brd85scbk446d2epl5k9r3qtld9g45mhtinhmknt6enchehgoejcqgp5984mqhl5darah163rlbd4nsgqj7vspa9kct2tb02ulefc2v5bed3va1v1fvj090p",
"SP3B":"0210s50erjjr22cpd42ltdn72mpgml65bdcu6lbnge7f90f78f0kru9jq4hf9t7spha058tba52l31a626bsenn7jafobpmeg9jai9gfrdj3h9j7hphicbqrhnf569k7g5isk8ai4lm33ftjmtpv24no5bp2odqda9pgs50erdjh2tt4hm4vtjk7h5udm0tf",
"SA1A":"09lbqf24bh2f26qblt47ajpd4eb8494prg0i6hq65075k03e7ru6un8p6mpqagj7l1am61bsbj05tdbuf94ncdfbs4ee1qdpghq0mnh84a49jvn4ifl1oekom95cofnp7ir4q878d4qdj8jpqvcq6csfdbq0t47stmtsenhiu2761mvvotlf",
"SA1C":"1cbet11f269v5knk8e5tvtcg6d0hii92ns5f9hs4fna0k6loahlg8latl3jjcdlmhrcdpujvd1eqa9d984qqdn64phdt2v2hhr371rju6cb7ljk0hblocsbpmkhlhgn25vu9nage7pnspiemo8m5oiong119p9v9c0svplneu13kc0vj3pf2",
"SM3B":"4cuhb662pap38lj1jd8nudek92j07vb11bajb780iqbe5dsibl1tsrbvapt0ct8mque75cfverq3k4fls45mh8r380hf590ojfesi5jh4sh7kglfbqm7fmquo29endoflslbpbpurqpnm3q6qqkivas0oc02sl9lv3sqnn2summvikv93pbpvn2dovvo37at",
"NV3E":"16p22e1s165i3fbglv3usqcn4ha5i75u62496tticn7b2hjs7j96ck81t24n8cr8lt9aob7h9hv96mbectutdc9783div1idgjl9ffi6or0jlshrppngpreuo0rtekooamm8ooqkm0os4blfrkl441s3l074svettote65aftvg5p2ufviaq",
"IV4":"32a78k",
"SP2D":"09vp8v19duif1gks4h33kgh33t56td4eejnp4ll72q5v0egj606tok6nsgpj72tjt1820krr91kesdaajdnhb80mqbbjuqitcg6m6lir3umdit7j8ejd6h1kjjaq4rjsam4pkm541gllsv3ulmjuf6m3u7v8n2gcqmpbfse7pdleshs80d6jvb5nf1",
"SPR1A":"4jmmnn5mtc1c7hvofga59on5amuqhriilmo8lqvmjanvq463sconoastg9lptt8bsa",
"NV2E":"0r9eh71ud5sm2av0to2mct3c3l7q8u6fik02aruov1c3us3hefu38cf0c2k5fi678afis8s2fl5pncgnhfaigqp4g1hhdodoi0gponjo1r6gkoh91blkcgd8mm9i0qmt94i8omndtfp9o4lhpkq6vkppvdliqi6o5cs69hr2su552bsvvopsv36v02",
"SM3C":"027n4k1u497k33pdvr4ptt246to78g7aqrrl84ca509c89a29r9ocba45dqha5aa5oari5ojbq2vefe6kl77evaf3nfuvkurgdu185h7lugphbtf0qjm88ldjqnpd0jsnfi2mbpcr6nvvblloilfv0oudnl8pbe34dr7pa9it576i6upuf7kv92k3o",
"SA1D":"1c12tm1ofrm02sq8f25einko77crrp7i76u7cj6icfclmjijcr5fuiehkkiaehpii3h74sdrhkrvscjq4t9vjr7ca9kce39rlhqstvlvs4apmc19b8mi73t9o179c0p2vfu5q18k8fr9dn57sivu6ksse6anu0s5bnu7p9r7u9sa0vvp5i6a",
"IV1":"4cls7kf5r4sa",
"IV3":"p9ueu8"
};

// User names (36kB).
var userNames = {
	"642370865":"Amina Abdallah Abdo","560591079":"Sussan Abdi","611885348":"Liban Abdulahi","132493216":"Sarah Abdullah","21760182":"Yasmin Abdullahi","770570516":"Nadja Abisman","552178792":"Anton Abrahamsson","36659936":"Sara Abreham","903255603":"Abd Al Rahim Adawi","146759508":"Christopher Adolfsson","40695167":"Ida Adolfsson","973631198":"Oscar Adolfsson","187067395":"Andreas Af Trolle","615028655":"Karl Agell","404984466":"Pontus Aggesund Karlsson","546023378":"Anna Ahlén","360017497":"Pål Ahlsén","328548805":"Oskar Ahlstedt","191990136":"Mimer Ahlström","322875343":"Noa Ahlström","651710693":"Rahmatullah Ahmadi","768841965":"Sara Ahmadiyan","82447248":"Mustafa Al Genaby",
	"37844560":"Hajer Al Saleh","1014566699":"Tishko Al-Dalo","643016935":"Mohammed Al-Taee","692079809":"Daniel René Alarcon","334881290":"Caroline Aldenroth","643297791":"Nardos Alemayehu","223197064":"Karl Alexandersson","1038650675":"Nathalie Alexandersson","928259035":"Abdiwahab Ali","110649484":"Ali Ali","578345812":"Dalia Ali","222644048":"Hussain Ali","391445864":"Mohammad Ali","64826412":"Omar Ali","60725339":"Alice Allenius","836225763":"Anna Allert","841842446":"Andreas Allinger","1003466119":"Julia Almebäck","584328913":"Matilda Almgren","167545146":"Carl Almquist","55352899":"Amanda Almstedt Valldor","903185427":"Ebba Alsterlund","907654552":"Kristian Alvarez Jörgensen",
	"978300908":"Jakob Alvelid","760312084":"Daniel Amani Rudäng","1054809112":"Roy Amerek","726975303":"Nilab Amiri","220560010":"Agnes Andersson","217446189":"Eric Andersson","225628083":"Henric Andersson","12374903":"Johan Andersson","883873944":"Love Andersson","820622160":"Mimmi Andersson","2130821":"Nico Andersson","1035302175":"Axel Andersson Lindh","523031560":"Adrian Andersson Martvall","494188486":"Ebba Andersson Nording","882520802":"Albin André","1060694975":"Stina Andreason","455918127":"Martin Andria Habchi","722183033":"Angelica Ankarberg","869879452":"Sara Antar","286798813":"Linnéa Antman","140029999":"Karokh Said Araf","915732552":"Marcio Roberto Arango Torres",
	"947301215":"Amanda Arenander","652844520":"Felype Arevalo Arancibia","141905709":"Alma Arnek","855440801":"Linda Aronsson Taxén","708717257":"Peter Arvidsson","1049042699":"Danna Asad","362694955":"Can Aslan","889296819":"Julia Aspevall","269431354":"Imane Asry","801950166":"Sofie Astner Jansson","774045641":"Ella Attling","204253987":"Petra Attling","920241245":"Erik Avebäck","725221931":"Freja Avebäck","437198068":"Simon Averin Markström","122127713":"Oscar Backlund","928245059":"Maten Baderkhan","270436503":"Dylan Baghbani","863702854":"Peter Balazs","876830757":"Mick Baltinger","474613713":"Christian Barekew","598155245":"Abdulkader Baroudi","74320186":"Feras Baroudi",
	"134901162":"Daniel Barriga","372799694":"Karolina Barrios","103095572":"Hugo Barsne","819803465":"Lawend Azad Barwari","32168643":"Helin Baskalayci","322769455":"Frida Becker","104481315":"Erik Bejmar","919643932":"Robel Bekele","952917770":"Fatima Belgacem Houri","1042989352":"Ofelia Belkacem Edvardsson","542667891":"Emma Bellander","479889978":"Jorge Alberto Bello Arones","201553684":"Marika Bendes","1066334540":"Johanna Bengtsson","319516596":"Lotta Bennert","951275976":"Arthur Berg","202214954":"Hannah Berg","1050343419":"Julia Berg","54434060":"Sofia Berg","407456389":"Rasmus Bergelv","267499981":"Amanda Berghel","993194180":"Kajsa Bergklint","547481697":"Sofia Bergkvist","598551424":"Oscar Bergling","1073510063":"Filip Bergljung","854451184":"Johan Bergljung","723427354":"Herman Bergman","595645243":"Julius Bergman","595208760":"Oskar Bergman","15416492":"Patrik Bergman",
	"654097261":"Erik Bergquist","926122705":"Ylva Bergqvist","173700464":"Ruben Bergvall","225096645":"Felicia Bergvik","625613055":"Blien Berhane","508144994":"Evelyn Berkeley","1000753228":"Linnea Berndt","525526764":"Erik Bernebrant","898564755":"Lukas Bernhard","830948449":"Ebba Beskow","655520295":"Marcus Bigert","394382826":"Ellinor Bill","797093666":"Tobias Billing","535693471":"Emma Bjelke Blomqvist","892394460":"Amanda Bjurenborg","526521657":"Carl-Michael Bjälkensäter","28621351":"Åsa Björck","462808244":"Love Björk","232529168":"Isabelle Björklund","163282738":"Marc Björklund","242642809":"Julia Björkstedt","58953577":"Rickard Björkäng","561443572":"Rebecca Björn","890606545":"Pontus Blomberg","667663938":"Albin Blomkvist","164709124":"Markus Blomqvist","1049710712":"Hanna Bodin","694325296":"Sara Bodin","1027263869":"Jonatan Bodvill","13683779":"Andreas Boijsen",
	"245840527":"Adiltogtokh Bold","798215012":"Per Boltegård Blom","215530147":"Bruno Boman Erlandsson","1031993640":"Casper Boman Wiberg","298966316":"Fredrik Borgström","957172991":"Olivia Borgström","162374423":"Max Bornehed","156933210":"Ilona Boros","440170366":"Tuva Borup","141651875":"Maha Boukadida","943358180":"Ken Braesch-Andersen","1044277153":"Noah Bramme Fall","125610418":"Karin Brandt","131223732":"Alexander Bratt","679823509":"Elsa Bratt Lyssarides","853195665":"Olivia Bratt Qviström","968999607":"Nathalie Bremås","113883468":"Joe Brereton","57247429":"Johan Brishammar","416995899":"Malin Brorell","174550261":"Victor Brunzell","79197850":"Henrik Brüllhoff","630925505":"Maja Brädefors","587493487":"Artur Brändström","545824000":"Daniel Buchberger","141277833":"Sanel Burgandinov","939490298":"Fredrika Burvall","750564198":"Matilda Byqvist Nilsson","906627205":"Einar Bäckström","449092867":"Roland Bäckström","1044160608":"Viggo Bäckström","807763841":"Wilma Bäckström Nilsson","502845098":"Arvid Börgö","97329634":"Anton Cala Huurtela","349020603":"Felicia Cantarero Nevalainen",
	"709760256":"Leonard Carlén","184753501":"Sandra Carlson","12984497":"Tilda Carlsson","138374017":"Cristian Carmona Puente","1043561474":"Ying Cassel","729874221":"Philip Casserlöv","805114051":"Sebastiana Castillo Rojas","1015122093":"Emma Cedborn","224105897":"Oliver Cedenheim","264588057":"Emma Cederberg","555549399":"Jim Cederlund","284185470":"Jacob Cederqvist","716783124":"Joseph Chaouch Båth","419669179":"Sajaval Choudrey","509449098":"Alva Christensen","140182197":"Tara Christensen","980034737":"Christoffer Cialec","690831614":"John Claesson","829142959":"Loke Claesson","592314252":"Sofia Claesson","494640746":"Freja Colliander Ikeda","298172832":"Sultan Corap","781620554":"Ilian Corneliussen","17365590":"David Crabtree Gärdin","937790312":"Sinan Dagli","618449536":"Martin Dahl Becedas","881041828":"Hanna Dahlander","784264007":"Adam Dahlberg","690304164":"Filip Dahlberg","389027757":"Love Dahlberg","850915783":"Max Dahlberg","77114039":"Natalie Dahlsten",
	"769863960":"Axel Dahlström Klint","1021180457":"Jonathan Damot Desta","621640207":"Emma Danell Lindström","870034188":"Ronja Danell Lindström","616483454":"Emma Danielsson","517510166":"Isabell Danielsson Sandegarn","397615669":"Arvin Daraiee","893752732":"Maria Darle","433605857":"Ruben Das","376420485":"Hjalmar Davidsen","86664018":"Jakob De Vries","1046634945":"Agnes Deak","245560958":"Agnes Deckel","340227338":"Marcus Dekker","851245946":"Emma Delin","579184112":"Axel Demborg","253991879":"Nils Denward","389491412":"Rahel Desbele","1020864885":"Cheima Dhahri","314808800":"Josephine Digné","459803948":"Fayes Diha","619152562":"Vassiliki Dossis","151276527":"Jesper Doyon","338212555":"Nemanja Dragoslavic","978255687":"Nike Drakos","70945881":"Salsabil Dridi","860691444":"Meimei Du","786264442":"Brandon Duarte Tsegai","1031890177":"Stina During","334089245":"Ihsan Firat Dölcü",
	"252899810":"Love Edander Arvefjord","651987141":"Leopold Edling","29502909":"Joakim Edlund","42618494":"Ville Edlund","123087238":"Amanda Edlund Springer","437196850":"Johan Edstedt","717342952":"Carl Edström","829948969":"Erik Edström","512051304":"Daniela Edström Erlund","70568111":"Hedvig Edvall Bons","623670119":"Alan Eftekhari","318904085":"Martin Egholt","972519352":"Agnes Ek","1058576137":"Alice Ek","946040161":"Lina Ekebrink","463805043":"Sanna Ekegren","574955686":"Jonah Ekerman","780110716":"Daniela Eklund","1020986287":"Fredrik Eklööf","294869818":"Malin Ekstrand","884354021":"Claudia Ekström","459979454":"Lukas Ekström","147801446":"Andreas Ekström Cardsjö","1004102956":"Graziella El Ghorayeb","17989186":"Felix Elfgren","46173110":"Emelie Elg Clason","701253475":"Nayomi Elias Bairu","658213162":"Rebecca Elvbo","880854915":"Vendela Elvbo","74264253":"Isabelle Enberg",
	"498929284":"Linn Enekvist","325523191":"Oscar Engberg","705987727":"Camilla Engström","526203462":"Sofia Engström","1055998190":"Filip Engvall","600306300":"Karl Enoksson","579331361":"Robin Erdal","61147267":"Edvin Eriksson","176741008":"Isak Eriksson","404454631":"Linnea Eriksson","305262022":"Magdalena Eriksson","647167868":"Olivia Eriksson","480094254":"Oskar Eriksson","976137218":"Sara Eriksson","730231161":"Melvin Eriksson Schröder","821904198":"Alexander Erneborg","364451603":"Mattias Ernefors Zettergren","778302076":"Ninau Ernström","967313660":"Philippa Esbjörnsson","370794428":"Viktor Espinoza","582925338":"Samuel Evrén","629631836":"Jacob Ewertzh","337385988":"Makda Eyob","757305257":"Bella Ezimoha","366644851":"Isabelle Farrell","58283354":"Nikta Feiz Barazandeh","814421778":"Sabina Fernandez Rojas","11241818":"Amanda Fernando","466713681":"Elsa Fichtelius Boye",
	"895582672":"Miriam Fidler","618282282":"Sofia Flores","489433719":"Carl Flyg","585786173":"Albin Flygare","223731214":"Stina Fors","974421794":"Albin Forsberg","866105010":"Anton Forsberg","628459939":"Arvid Forsberg","377510731":"Fabian Forsberg","658370453":"Martin Forsberg","644245982":"Viktor Forsberg","2038983":"Michael Fosselius","729662892":"Simon Fransis","578955401":"Viktor Fransson","786454063":"Nicholas Frederiksen","756992753":"Sofia Fredholm","986488789":"Rasmus Fredrikson","284812417":"Alvin Freund","716514277":"Gustav Friberg","391182235":"Olivia Frick","166075437":"Fanny Frimodt","242864166":"Samuel Frisell Ellburg","541920587":"Alem Fsaha","65443734":"Caroline Fura","906267662":"Karl Furehed","863223735":"Simon Färegård","715914391":"Javzanpagma Ganbaatar","651996227":"Krishna Gandhi","593615026":"Tova Ganellen","566543134":"Jakob Gauffin","767536499":"Rikard Gauffin",
	"645814713":"Salem Gebreslase","102844528":"Akilles Georlin","830823112":"Carola Gerges","985151517":"Arsema Gersus","154884855":"Yodit Hailemare Getachew","1029127554":"Robin Gezang","563924640":"Daniel Ghazarian","599792264":"Henok Ghebremedhin","585776562":"Rabab Ghoul","381642740":"Isabella Gigante","837213864":"Anton Gille","850857101":"Karl Ginsburg Hultman","507425941":"Sebastian Ginsburg Hultman","523954703":"Miranda Gisudden","886678137":"Hanna Goldman Taguatinga","617262292":"Arslan Golic","993664549":"Emelie Grahm","426244840":"Petter Granath","30926949":"Mathias Grape","325218553":"Anders Green","977531830":"Simon Greenhill","280816178":"Emma Grip","682494644":"Erik Grundberg","422791567":"Maja Grundskog","181758696":"Niklas Gruvman","58840579":"Fabian Grytt Österman",
	"31821097":"Chouaib Guediri","702533785":"Elin Gustafson","797188057":"Felix Gustafsson","342255966":"Frida Gustafsson","681414493":"Malin Gustafsson","589705051":"Philip Gustafsson","376701530":"Sofie Gustafsson","428399017":"Viktor Gustafsson","268510164":"Johanna Gustafsson Flink","552527986":"Andreas Gustavsson","311823765":"Chris Gustavsson","984485474":"Fredrik Gustavsson","581374814":"Hanna Gustrin","610741759":"Nathalie Guzman","429003316":"Leylafer Gümüs","593405074":"Leo Gynge","827058562":"Efe Gürgün","468673627":"Annika Gåling","226378547":"Karl Gåsste","11176402":"Moa Göransson","477278719":"Ylva Göransson","538658074":"Per Görts","75090279":"Renée Göthlin","677083253":"Viktor Haber","832389560":"Esmedin Hadzic","909088860":"Victor Haeggman","586906508":"Hannes Haglund",
	"845399546":"Josef Hagos","995288808":"Martin Hahn","949849255":"Marc Hahne","70631912":"Miriam Haile","376723092":"Marieh Hajbarati","486483290":"Alva Hall","966076044":"Karolina Hall","798315816":"Jessica Hallerth","714665471":"Oskar Hallinder","107804120":"Chani Hamasdi","860699327":"Chiheb Hamdi","419649749":"Sally Hammar Persson","852110974":"Emil Hammarberg","465081351":"Sigrid Hammarlund","187342666":"Emil Hammarström","438789797":"Beatrice Hamnström","1069975523":"Hardi Hamshin","1003761546":"Mohamed Hamud","1060602698":"Saskia Amanda Handberg","88326357":"Dardo Asia Hanson","579702016":"Freja Hansson","769457220":"Joakim Hansson","353762994":"Moa Hansson","387068361":"Runa Hansson Bittar","56019708":"Matilda Hansson Nord","119357791":"Linnea Hargeskog","524607467":"Hamza Harrami",
	"143019287":"Sebastian Hassel","780661842":"Elias Hasselberg","664072846":"Samuel Hassler","806412745":"Love Hedenvind","309335449":"Laura Hedlund","199226768":"Didrik Hedqvist","849296328":"Felicia Hegethorn","637383197":"Emelie Heidling","844406934":"Chelsie Heller","8384017":"Pontus Hellerstedt","126022201":"Daniel Hellgren","761198024":"Henrik Hellström","834809519":"Katarina Hellström","1007527091":"Terese Hellström","102320868":"Gustav Hellströmer","1051909601":"Rebecca Henriksson","988259767":"Alexander Heras Veriñas","90093287":"Johanna Hermansson","636002028":"Tomas Hermansson","704877875":"William Hesselstad Vardon","869564934":"Susanna Hestréus","432699418":"Melvin Hidesäter Bredell","827188619":"Christian Hildebrandt","250905063":"Tobias Hildebrandt","347492473":"Saga Hiller","682601372":"Louise Hjelmqvist","412775296":"Alexander Hoang","231889742":"Victoria Hoang","243185843":"Maja Hofmeijer","510086131":"Anton Holmberg","376943890":"Marcus Holmberg","694610464":"Edvin Holmerin","332657457":"Charlie Holmsten","566102400":"Alexander Holzmann Ekholm","320840918":"Louise Homle","90600556":"Paulina Huang","705211894":"Olivia Hultgren","957062221":"Hanad Huseen","722984996":"Ebba Huss",
	"341907836":"Darko Hussein","1018492592":"Fredrik Hybertz","764284928":"Isabell Håkansson","269811804":"Olle Hålén","460425646":"Peder Hårderup","784906800":"Emelie Häger","890934698":"Alva Hägglund","546941513":"Madeleine Häggquist","561303695":"Rebecca Häggquist","118798664":"Jonathan Hällström Sander","17540535":"Elias Händig","1003878180":"Calle Höglund","617001508":"Johanna Hörnell","598842427":"Ida Hörner","320494020":"Nathalie Ilehag","978294150":"Yann Irigoyen","40869856":"Ahmed Shukri Isack","321670424":"Emma Isberg","181205906":"Frida Isgren Lagerås","481464379":"Muhyadin Abdi Ishaq","814853552":"Elias Iskos","406810737":"Nikita Islam","255105428":"Ali Issa","1036062030":"Even Shamoun Issa","1059715854":"Beatrice Isvén","670904167":"Ari Jaakola","304709887":"Sally Jacobson","293066162":"Wisarud Jai-On","504877986":"Jonas Jakobsson","507917985":"Emil Jangelind","758096239":"Billy Jansheden","589288385":"Emma Jansson","887084188":"Samuel Jansson","590535235":"Gustav Jansson Dahlberg",
	"853754534":"Piotr Januszewski","944166598":"Sarah Jasim","656416641":"Jakub Jasinski","564957697":"Alexander Jegebo","657205734":"Anton Jensen Wennberg","291079789":"Sofia Jernberg","1016997919":"Emelie Johansson","339916625":"Emma Johansson","204035967":"John Johansson","574038539":"Max Johansson","774386518":"Casper Johansson Nordqvist","526237832":"Fanny Johnsson","643601129":"Elin Jonasson","207515725":"Fanny Jonasson","638434504":"Oliver Jonasson","433347422":"Ida Jonsson","578484761":"Roila Jonsson","51178674":"Adrian Jonsson Sjödin","712378319":"Isak Jonsson Sjödin","44471598":"Frida Joona Nyström","176540153":"Gordon Jordell","131242925":"Jakob Josefsson","1060119577":"Marc Jullings","602833455":"Axel Jungell","688569722":"Sebastian Jungstedt","221997480":"Elias Jürgens","1020433916":"Angus Järneström Nääv","668713435":"Jack Järneström Nääv","151843446":"Peter Jönsson","117519710":"Sofie Jörgensen","172724267":"Egil Kahlbom","232704407":"Erik Kaiserfeld Fonser","64960700":"Mary Kalantarian",
	"431144914":"Marcus Kaliszczak Sörensen","825654957":"Ioanna Kalpakidou","1042636509":"Nicki Kalpakidou","113843494":"Minette Kannerberg","214976082":"Sandra Kappelin","26822116":"Nils Karanta","522394890":"Jonathan Kardelind","523779161":"Andreas Karlin","891510956":"Arvid Karlsson","863640028":"Emil Karlsson","741457843":"Magdalena Karlsson","133508569":"Sara Karlsson","83033811":"Simon Karlsson","611085331":"Valdemar Karlsson","1012268586":"Victor Karlsson Valentin","402418605":"Nelly Karlsson Åhlén","396459471":"Linnea Karnéus Hall","748336126":"Anna Karreskog","333754296":"Christer Kassar","911004991":"Ellen Kavéus","909260604":"Zohal Kazemi","607958521":"Sinthia Kazi","907260528":"Abel Kebede","996781124":"Rahel Kebede","1007661089":"William Kenttä","647842719":"Jacqueline Kessidis","1020537784":"Emil Keteli Castillo","312055892":"Anas Khan","64622433":"Tomas Kidane","45535293":"Ishak Kilic","632421069":"Ronya Kilic","244042808":"Sofie Kilic","865200002":"Axel Klasson",
	"365819012":"Zac Klerck","364864481":"Henrik Knudsen","281911997":"Christina Koidis","779101698":"Patryk Szymon Kois","1029177823":"Georgios Koroly Ergatis","200292883":"Isak Korpskog","759952590":"Jens Korsvold","556380501":"Arsam Kourvand Takht Sabzi","537051962":"Gustav Krantz","114287434":"Jonna Krigsman","880097076":"Axel Kristola Truc","704722656":"Linnéa Kronberg","561717403":"Andreas Kubicek","959445204":"Ludvig Kullén Dansarie","244933482":"Arian Kurti","339802193":"Amar Kurtovic","821752491":"Miranda Kvarnlind Lucki","40667214":"Sara Kviberg","30311985":"Lukas Kvist Wiberg","69625252":"Isabelle Käller","301571130":"Simon Källkvist","147517684":"Joanna Källström","106986781":"Mats Källström Karlsson","109247809":"Elias Köhler","62953650":"Niklas Könberg","240779374":"Emil Körnung","336904500":"Ludvig Ladeby","149376761":"Andreas Lagebäck","997672410":"Oliver Lager","670798415":"Anton Laguna Ek","825392940":"Magnus Lam","843010805":"Vanja Lamm","362129694":"Björn Langborn",
	"553498211":"Emil Langlet","750822760":"Alexandra Larsson","728365542":"Camilla Larsson","133095859":"Caroline Larsson","672339634":"Edvin Larsson","509206828":"Jenny Larsson","161412164":"Jessica Larsson","1001246559":"Joar Larsson","182232046":"John Larsson","883040128":"Mattias Larsson","604530455":"Michaela Larsson","478513791":"Mikaela Larsson","123149816":"Oscar Larsson","1060739463":"Rasmus Larsson","623654236":"Victor Larsson","397844668":"Hanna Larsson Werle","671526090":"Oskar Lavelle","295283840":"Axel Lavenius","797940279":"David Lazarevic","6239402":"Denise Lekare","1055744693":"Elin Lemon","337150109":"Viktor Lemon","787337911":"Vendela Lenberg","310931332":"Alexandra Lennartsson Nordman","484501945":"Emma Lenngren","283471424":"Emil Lepikko","568614229":"Edwin Li","245090929":"Hannes Lidbrink","10478879":"Alice Lidén","697051438":"Dan Lidholm","280771275":"Rebecca Lidman",
	"972871406":"Olivia Liljedahl","216440248":"Karolina Lind","905341132":"Markus Lind","95869895":"Rebecka Lind","476671156":"Mikael Lindahl Valdenström","583142212":"Adam Lindberg","388938891":"Amanda Lindberg","686695269":"Emil Lindberg","440490015":"Jacob Lindberg","1023870869":"Jakob Lindberg","831345344":"Johan Lindberg","914139442":"Nicki Lindberg","927633537":"Nelly Lindblom","806593920":"Alice Lindborg","314844482":"Anna Linde","139342817":"Ludvig Lindeberg","706172285":"Emma Lindelöw","840343756":"Johan Linder","694535377":"Gustav Lindfors","1007800704":"Emma Lindgren","987664449":"Josefin Lindgren","96715194":"Leo Lindgren","233739851":"Robin Lindgren","940693262":"Johanna Lindh","580492857":"Simon Lindh","62998951":"Jonatan Lindholm","531757658":"Simon Lindholm","718371271":"Christine Lindmark","542699374":"Ludwig Lindmark","287153993":"Fanny Lindqvist","86752129":"Oscar Lindqvist","265421008":"Dennis Lindström","346666737":"Fanny Lindström","506893237":"Linnea Lindström","12718566":"Patrik Lindström","761076058":"Peter Lindström","47099658":"Oskar Lingnert","673055795":"Sofia Linjo",
	"807713517":"Nelly Linnhag","551070019":"Hampus Livfors","178465678":"Fanny Liwenborg","838060327":"Joe Ljungmark","716803488":"Simon Ljungqvist","556733546":"Edvard Lodin","526258358":"David Loforsen","770445943":"Frida Long","1029557":"Angelo Lopez Zumaeta","350036767":"Gisella Lopez Zumaeta","178715152":"Hampus Lund","300483224":"Niclas Lund","792357484":"Dante Lundahl","80084415":"Erik Lundberg","257740129":"Fabian Lundberg","657772035":"Felix Lundberg","588702136":"Kaspar Lundberg","747811599":"Louise Lundborg","270553979":"Anna Lundborg Regnér","651379764":"Hannah Lundgren","1057329263":"Kalle Lundgren","10137423":"Axel Lundström","955690816":"Andreas Luther","936363242":"William Länk","1023396186":"Christian Löfgren Utne","354074901":"Josef Löfvendahl","751824868":"Josephine Lövgren","858432599":"Klara Lövgren","871553313":"Jonathan Lövholm","536054408":"Alex Löwdin","230016153":"Hampus Löwstedt","748178578":"Åke Lööv","320737783":"Daniel Mac Carthy","571515326":"Patrik Machnow Sjöberg",
	"661650545":"Viktor Madenteg","303709069":"Ehab Mageed","1051375708":"Simon Magnusson","450448020":"Bardia Mahjoor","814948302":"Anna Malek","634637582":"Anna Malm","7561960":"Lovisa Malmberg","640779369":"Lukas Malmgren","978557510":"Fabian Malvet Rydell","902068707":"Simon Mannerheim","15584891":"Theodor Maric Larsson","928466453":"Isabella Marklund","789376478":"Rebecka Marklund","440976031":"Kevin Marsden","657719232":"Alexander Martinez Eriksson","352951969":"Isabelle Martinez Sundin","646403897":"Albin Matson Gyllang","1032280602":"Emil Mattsson","696820779":"Linde Mattsson","18016838":"Ville Mattsson Kjellqvist","354054756":"Amina Mboge","715556635":"Harald Melin","517255353":"Max Melin","351085050":"Elin Melin Hamber","149266569":"Alva Mellgren","550293428":"Evdirehman Mercan","104642555":"Mikaela Merilä","753852989":"Jesper Mesilane","1026534646":"Johanna Michaelsson",
	"686985211":"Dani Mikael","441797777":"Elin Mikaelsson","374065190":"Mattias Milger","469152692":"Karolina Milutinovic","118117710":"Razwia Mirkhani","146579293":"Sohag Mirza","652903327":"Matyas Mitro","761579546":"Shayan Moazeni","264182743":"Lars Moberg","878763415":"Mathias Moberg","161287433":"Marcus Jonathan Modin","191803436":"Ahmed Abdirisaq Mohamed","871014911":"Farhia Abdirisa Mohamed","15236132":"Gholam Mohammadi","210011327":"Atid Mohammed","80592169":"Anisa Mohammed Ali","341762942":"Elin Moisander","392541608":"Jakob Molin","1017807371":"Axel Monthan","1037753985":"Tomas Moraga","726024616":"Sofia Morin Lantz","293549524":"Jonna Mosesson","575725860":"Miriam Mosesson","488426051":"Anastasija Mosina","385833000":"Driterona Mulaj","940322818":"Amanda Muljadi Hylander","594688995":"Zeineb Mustafa","627782653":"Jacques Mwe-Luemba","246247285":"Albin Myrberg","873746703":"Joanna Myrman","782776569":"Angelica Mångs","46513057":"Hugo Månsson","428053496":"Sofie Mårdh","472793783":"Frida Mäkelä Strandlund","237948833":"Ebba Mökjas","1061220847":"David Möller","97619244":"Filip Möller","705165918":"Oscar Mörke","454147230":"Sango Nahimana Sabiti","54556701":"Siva Narayan Nalluri",
	"167864201":"Mariam Naziri","745258587":"Marthe Ndiwa","240729519":"Erimias Negasi","342660393":"Martin Nelander","586327064":"Rasmus Nelzén","401958816":"Maria Neuman","122341671":"Albin Niemelä","577595647":"Abiel Niguse","695553832":"André Elias Nilsson","987960655":"Erik Nilsson","747602043":"Erik Nilsson","510053779":"Frida Nilsson","470844847":"Henrik Nilsson","525456053":"Johan Nilsson","915856551":"Josefin Nilsson","425414227":"Karolina Nilsson","374716488":"Olav Nilsson","902624007":"Wiktor Nilsson","7671913":"Erik Nilsson Hansen","592900361":"Fabian Nordell","659536139":"Marika Nordenström Jung","202880878":"Tilde Nordh","969349463":"Hedvig Nordling","725639613":"Samuel Nordlund","491325130":"Ville Nordström","336747020":"Jesper Nordzell","842048912":"Emil Norell","503712389":"Kalle Norgren","152376903":"Anna Norrblom","1037252200":"Amanda Norring","717731175":"Erik Norström","71681103":"Sabina Novo","481570146":"Jessica Nunes","928363671":"Lukas Nyberg","170478113":"Rufus Nyberg Vidén","993505432":"Erik Nygren","591135377":"Niclas Nyholm","384650036":"Mirza Okanovic","965842737":"Leon Oldermark","933041950":"Martin Olofsson","22604231":"Lisen Olofsson Rydmark","436710574":"Erik Olsson",
	"502611063":"Fanny Olsson","681033733":"Matilda Olsson","844961215":"Alexander Olsson Nelin","85089351":"Amanda Olsson Nelin","219993457":"Ismet Omerovic","488264266":"Olivia Onwuta","114104088":"Miranda Orbelius","486477068":"Gustav Orbring","782300433":"Anthony Orfila","964644711":"Hanna Oskarsson","65621248":"Lina Oskarsson","351213544":"Amany Osman","390031965":"Sanna Ottosson Fix","719472357":"Tony Paananen","512009738":"Stefan Palikuca","29420952":"Tova Palm","537779946":"William Palmén","955811619":"Gabriella Palmqvist","217665538":"Carla Pardo","273496778":"Golpar Parvardeh","1064384287":"Frida Paulsson","501767274":"Kenneth Perera Van Der Wall","348927828":"Vera Perrin","806221268":"Frida Persson","577925563":"Julia Persson","274015928":"Loanna Persson","729240685":"Sally Persson","738303705":"Josefine Persson Flisager","370191699":"Anna Peterson Frid","764600882":"Ella Peterson-Berger","486147813":"Katarina Petrovic","842533687":"Frida Pettersson","561122326":"Isak Pettersson",
	"692023498":"Jesper Pettersson","351195485":"Mårten Pettersson","682333114":"Aksel Pettersson Naumann","359623227":"Naphaskorn Phaolaman","530820654":"Simon Pierce","437741897":"Thea Pinét","975447868":"Mimmi Pinto Guillaume","305231992":"Matias Piqueras","200260287":"Emil Pirverdiyev","768905800":"Patcharaphorn Polprasert","528869070":"Piotr Popowicz","1033846004":"Felix Porsner","626930858":"Victoria Portocarrero Rodriguez","653750853":"Petronella Pousette","718585989":"Frida Prestjan","280650054":"Eric Pruul","435907696":"Anton Pålsson Abrahamsson","1002921522":"Jones Qanbour","247850116":"Joakim Qvarfordt Hammarlind","697969485":"Murtaza Rafi","359603402":"Erik Rahem","199428233":"Patrik Rahimzadeh William-Olsson","458933152":"Victor Rahimzadeh William-Olsson","183066264":"Sophia Rahmani Azar","529176580":"Daniel Rahme","414758310":"Jessica Raikunen","79796898":"Johan Ramn Alenius",
	"726698700":"Gabriel Rapp","1070581754":"Linnéa Rasch","109333606":"Rickard Rasmusson Tollstoy","579770690":"Tobias Rastemo","664958271":"Angela Refuerzo","669200743":"Lotta Reiman","403584582":"Jens Reimers","513759755":"Philip Reimers","1065459037":"Lisa Carolina Reine","336206520":"Alicia Remes","930683074":"Linn Rendahl","809376039":"Olle Rendlert","895481125":"Ali Rezai","234412211":"Jonatan Ribrant","856560654":"Love Ridderborg","701625607":"Carl Ridnert","1009602643":"Felix Ringqvist","526222209":"Nils Ringström","990322645":"Joshua Ristrand Bosyk","727068074":"Dylan Roberts","892006583":"Renato Bruno Rocha Negrao","16000678":"Linnéa Román","213522321":"Adam Rongione","861047103":"Carl Ronström","742334440":"Alice Roos","227159264":"Vendela Ros","660873901":"Julia Rosborg","725454227":"Fideli Rosell","269396123":"Jacqueline Rosenback Grip","53009290":"Josefin Rosendahl","841990085":"Sara Rosenkrantz","778691797":"Anna Rosqvist","178494701":"Hampus Rossberg","341436368":"Caspar Rossland Lindvall",
	"691671419":"William Rostén","666087753":"Lisa Routio Rutqvist","356200888":"Clive Rudd","785790328":"George Rudd","678958684":"Kristine Runelöv","341107429":"Emedi Rusongera","732945002":"Fatuma Rusongera","1050737711":"Malin Rutberg","1060234722":"Ellinor Ryman","836030119":"Fredrik Rålenius","969148484":"Sebastian Röhr","662760656":"Oliver Rönnkvist","51015825":"Emelie Sabel","857451844":"Mohammad Nader Sabery","609717292":"Nicole Saeed","579366633":"Kamyar Sagheb","23262381":"Habib Sahar","170250788":"Jacqueline Saidi","169225847":"Aleksander Salasoo","253747696":"Hakizimana Salima","335717291":"Liam Sallmander","572771746":"Muaamar Salman","532162984":"Leonid Salnikov","25861951":"Sara Samir Jebal","447038782":"Oscar Samuelsson","53627066":"Vanessa Sandberg","145362707":"Filippa Sande","117851117":"Edvin Sandén","664477899":"Ella Sandström Sehlin","678671774":"Isabelle Santiago","129217294":"Johanna Sarskog Steigleder","383811917":"Jonas Sartorius","746052446":"Joanna Sarvanidis","43448911":"Maja Sato Nilsson","634814853":"Aleksandar Savic","113896088":"Ellionore Schachnow","936641703":"Matilda Schaffer",
	"383956882":"Kevin Scheffer","167595998":"Sofia Schierbeck","805276571":"Anders Schill","303732014":"Adam Schlyter","748119374":"Philip Schlyter","330621323":"Ina Schmitz","64914396":"Vincent Schultz","291095453":"David Schwarcz","883435853":"Leon Sedvall","764762876":"Jonatan Seger","355560176":"Mikael Segerholt","672778600":"Louise Segerlind Hök","489463982":"Lisa Seglare","390459150":"Emmi Segle","42023472":"Sandro Seki","947926227":"Hanna Selin","389243212":"Evelina Sellén","416788356":"Maie Sene","122939678":"Caroline Sergo","965425385":"Aida-Sadat Seyed Yousefi","239988842":"Kevin Shahi Niri","381110553":"Enise Shala","665576864":"Nelly Shams","622239359":"Ibrahim Sheikh Abdi","313273585":"Dona Shemmon","92173157":"Ramitha Shemmon","276200903":"Yani Shen","29863437":"Eveline Shevin","930723450":"Mikaela Sigrell","805285557":"Ebba Sihlén","358811004":"Vilhelm Silfvergrip","1066584266":"Johanna Sillman","246498940":"Kasper Silverforsen","946172800":"Emil Simonsen Gedda","274738676":"Rosanna Singh","892164836":"Emelie Sjöberg","78611384":"Josefine Sjöberg Lundén","734943834":"David Sjöblom","424665431":"Jerry Sjödin","557701729":"Mattias Sjödin","164971330":"Ludvig Sjögren","971150411":"Simon Sjögren",
	"904502684":"Jennie Sjölander","1045805475":"Josefin Sjölander","314423648":"Sofia Sjöström Bedziri","740148511":"Moa Skan","324408452":"Vera Skans Mächs","1014802279":"Jim Skog Pirinen","926726240":"Fanny Skogh Larsson","14927327":"Oscar Skogman","530406036":"Sandra Skoric","736047435":"Ellinor Skymne","412564094":"Jacob Sköldmark","723350463":"Alexandra Smedman","1049748857":"William Smedman","485324517":"Anna Sokolova Ottesen","389154071":"Anna Solander","980155615":"Adam Solberg","1006114594":"Nina Sollier","338854186":"Viktor Sonebäck","178365076":"Adrian Soto Kronberg","1029751726":"Pantea Souri","865641687":"Waali Sowe-Janneh","667244697":"Henrik Sporrong","341125304":"Kevin Spring","210403741":"Miriam Stadler","414847110":"Aske Staermose Thiberg","220717145":"Niki Stajic Öhman","1072455960":"Louise Stenbeck","207378779":"Joel Stenhammar","224386048":"Olivia Stern","367945845":"Miranda Sterner","1046697980":"Sanna Stolperud","59240128":"Emma Strandberg","362718708":"Linnéa Strandberg","28948261":"Sofia Strandberg","220010656":"Nils Strandler","184342109":"Oskar Strandqvist",
	"824885216":"Linnéa Ström","1009482722":"Lucas Ström","738044156":"Marcus Ström","744829788":"Svante Strömberg","141671755":"Adam Strömdahl Östberg","273033376":"Emma Strömgren","820252490":"Johanna Suliman","923908690":"Sabrin Sultan Filli","849089201":"Emil Sundberg","24108954":"Isak Sundberg","559156099":"Albert Sundborg Horn","365664933":"Alina Sundell","380110106":"Magdalena Sundell","819480368":"Matilda Sundell","1049251128":"Claudius Sundlöf","417135231":"Elin Sundqvist","1030454868":"Bo Sundström","377507242":"Eric Sundström","362211983":"Johan Sundström","56446830":"Robert Sundström","458183734":"Fredrik Surell","904936502":"Ellinor Svahn","424228652":"Philip Svanhagen","315618105":"Lina Svedberg","78442902":"Martin Svedelius Myhre","753510100":"Agaton Svenaeus","5962739":"Gustav Svensk","777760590":"Emil Svensson","692407989":"Stephanie Svensson","1016652363":"William Svensson",
	"2350228":"Agnes Svensson Lindgren","946128738":"Teodor Sventelius","900806350":"Gordon Svärdsén","462134832":"Zeynep Süllü","967369243":"Hedvig Sylvan","917013699":"Linn Söderberg","849323392":"Nicole Söderberg Mothander","799963545":"Lovisa Söderkvist","95325759":"Mårten Söderlind","738070873":"Jack Södermark Neuhold","619939844":"Daniel Sörensen","1008965686":"Joel Sörensen","288802050":"Maria Sörensen","684301407":"Tugay Tabagoglu","401056919":"Joel Tagesson Råd","552888071":"Niklas Nikita Tagner","460526385":"Johanna Talamo","49355071":"Vendela Talenti","973071164":"Frida Tallström","209367985":"Sarah Taoudi","856065038":"Jonathan Tapia","681374079":"Petronella Taube","980372352":"Harde Tawfeek","753956345":"Joar Taylor","114986104":"Tova Tedengren Brenner","1002383924":"Erik Tedhamre",
	"296354323":"Roni Tekes","657811611":"Hermela Tekeste","615935647":"Sofonias Temesgene","304869551":"Junia Tenggren","466772569":"Alexander Ternerot","1031752195":"Timothy Tesstor","616473072":"Hanna Thelenius","183616032":"Martin Tholander","783798599":"Ronja Thor","600707682":"Josefine Thorén","398496061":"Johan Thunberg Jansson","220308555":"Gustav Thunvall","146042814":"Albin Thörn Cleland","69055570":"Rukiye Tirpan","195277639":"Alexander Tived","206943646":"Erik Tivenius","105084689":"Albin Toft","746303114":"Nora Tollin","23321905":"Adam Tomt","412291094":"Fabian Tomt","684133691":"Cassandra Torres Diaz","62044666":"Gonzalo Torres Salas","561560914":"Linnea Tounsi","970467001":"Fatou Touray","457656420":"Mariama Touray","595039433":"Matilda Tuovinen","551486725":"Maja Turesson","734588729":"Agnieszka Turska","65152244":"Teodor Tysklind","644585232":"Erik Tällberg","47172194":"Sofia Uman",
	"965847430":"Niklas Ung","304993774":"Johan Urberg Ramos","124974850":"Christian Utterman","1060054364":"Enkhzul Uyanga","1018387906":"Amira Vahlund Abdulkader","198512617":"Cornelia Valentin","915400548":"Ellie Valinger","759748144":"Andrea Vasquez Diaz","790189065":"Axel Vestberg","697462687":"Joel Vesterlund","967564380":"Moa Viberg","847117027":"Amanda Vickerfält","594764733":"Andreas Vickerfält","740579928":"Stig Viil","961861603":"Hedda Vitestam","804276209":"Rex Von Hage Taylor","384202717":"Gunnar Wahlqvist Odenman","771286719":"Isak Wahlund","56735281":"Mohammad Omar Waizy","553629795":"Pontus Walan","91013845":"Josefin Waldau","340874337":"Nathalie Wallenberg","706670509":"Morgan Wallhem","535811035":"Kalle Wallin","1062835494":"Mathias Wallin","93806218":"Felicia Wallnäs","797978708":"Anna Wang","994535620":"Michael Wang","523117442":"Wei Wang","606750645":"Jacob Wanselius",
	"306444659":"Fabian Waxin Borén","476730599":"Jakob Weiss","445911898":"Miriam Magdalen Weiss","133065111":"Kajsa Wernerson","1053586535":"Nora Wernerson","912302766":"Simon Westberg","210647378":"John Westerberg","925884684":"Jacob Westerdahl","374922835":"Linnéa Westermark","640360934":"Fredrik Westin","650202030":"Ivan Westman","384875787":"Erik Westphal","922834413":"Leo Wetterblad","442629739":"Force White","175675049":"Jasmine Whitworth","189518094":"Frida Wiberg","402781387":"Camilla Widén","557491695":"Sofia Widengren","1007555959":"Viggo Widoff Odeholm","8899522":"Ebba Wikberg","519628266":"Andreas Wilhelmsson","925557640":"Hanna Willart","602567890":"Erik Willberg",
	"977852575":"Jesper Wingbo","116201215":"Amelia Wirmén","28342648":"Herman Wistrand","26581485":"Kristoffer Wiström","116769471":"Jacob Wojtczuk","891520022":"Hampus Wranér","135717758":"Natalie Wrannvik","37744506":"Robert Wörlund","161333266":"Noel Yrigoyen Fernandez","569069216":"Yuk Keng Linda Yu","219407093":"Filsan Yusuf","727612542":"Jacinto Igor Zaitzewsky Micales","101819522":"Erna Zecevic","242804719":"Simon Zeidan Mellqvist","623565576":"Juan Carlos Zeledon Pinto",
	"296728888":"Anders Zetterberg","10472082":"Miriam Ziani","85299594":"Fred Zika Viktorsson","456099405":"Jasper Zimmerling","16126303":"Liyan Zou","212015210":"Stefan Åberg","701396587":"Katrin Åhlén","141917019":"Hanna Åhlund","699609122":"Joar Åkerberg","900353002":"Christoffer Åkerlund","711649876":"Per Åkerström","662902885":"Robert Åkerström","355297377":"Joel Åkesson","486003432":"Leonard Öberg Löfstrand","297873710":"Cecilia Ödén","816592526":"Martin Ögren","1057595807":"Adam Öhman","133668965":"Wilhelm Öhman","706493080":"Fabian Öhrn","950829425":"Johannes Ölander Gür","858226467":"Simon Ölund","106690686":"Burak Önsari","820145597":"Mike Önstedt","130835184":"Alice Örnelid","205350161":"Sara Österberg","953260881":"Nils Östervall","546856001":"Simon Östh","922837463":"Jesper Östlund","1024001217":"Sofia Östlund","383057167":"Molly Östlund Malmer"
};

// Decompress the above, and conversely map user -> class.
var userClass = {};
for (var cln in classData) {
	var clstr = classData[cln], cl = [];
	for (var i = 0; i < clstr.length; i += 6) {
		cl.push(parseInt(clstr.substr(i, 6), 32));
	}
	for (var i = 0; i < cl.length; ++i) {
		userClass[cl[i]] = cln;
	}
	classData[cln] = cl;
}


// Useful functions

var textProp = (document.textContent !== undefined ?
		'textContent' : 'innerText');
function getText(el) { return el[textProp]; }
function setText(el, text) { el[textProp] = text; }

// getElementsByClassName polyfill, modified from
// http://robertnyman.com/2008/05/27/the-ultimate-getelementsbyclassname-anno-2008/
var getElementsByClassName = function (elm, className, tag) {
	if (document.getElementsByClassName) {
		getElementsByClassName = function (elm, className, tag) {
			var elements = elm.getElementsByClassName(className);
			var ret = [];
			if (!tag)
				return ret.slice.call(elements);

			tag = tag.toUpperCase();
			for(var i=0, il=elements.length; i<il; i++){
				var current = elements[i];
				if(current.nodeName === tag)
					ret.push(current);
			}
			return ret;
		};
	}
	else if (document.evaluate) {
		getElementsByClassName = function (elm, className, tag) {
			tag = tag || "*";
			var doc = elm.ownerDocument || elm;
			var classes = className.split(" "),
				xhtmlNamespace = "http://www.w3.org/1999/xhtml",
				namespaceResolver = (doc.documentElement.namespaceURI === xhtmlNamespace ?
					xhtmlNamespace : null),
				returnElements = [],
				elements,
				node;

			var evalStr = ".//" + tag;
			for(var j=0, jl=classes.length; j<jl; j++){
				evalStr += "[contains(concat(' ', @class, ' '), ' " + classes[j] + " ')]";
			}
			try	{
				elements = doc.evaluate(evalStr, elm, namespaceResolver, 0, null);
			}
			catch (e) {
				elements = doc.evaluate(evalStr, elm, null, 0, null);
			}
			while ((node = elements.iterateNext())) {
				returnElements.push(node);
			}
			return returnElements;
		};
	}
	else {
		getElementsByClassName = function (elm, className, tag) {
			tag = tag || "*";
			var classes = className.split(" "),
				classesToCheck = [],
				elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
				current,
				returnElements = [],
				match;
			for(var k=0, kl=classes.length; k<kl; k++){
				classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
			}
			for(var l=0, ll=elements.length; l<ll; l++){
				current = elements[l];
				match = false;
				for(var m=0, ml=classesToCheck.length; m<ml; m++){
					match = classesToCheck[m].test(current.className);
					if (!match) {
						break;
					}
				}
				if (match) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	return getElementsByClassName(elm, className, tag);
};


// User management

function sameClass(user) {
	return (userClass[user.id] === userClass[meUser.id]);
}

function getUserLink(user) {
	return "/stockholm/contacts/users.phtml?edit=" + user.id;
}
function getUserLinkHTML(user) {
	return "<a href='" + getUserLink(user) + "'>" + user.name + "</a>";
}
function getUserAvatar(user) {
	return "/stockholm/contacts/myimage.phtml?view=" + user.id + ".gif";
}

function getUserFromId(id) {
	var name = userNames[id];
	if (!name)
		return null;
	return {id: id, name: name};
}

function getUserFromLink(uel) {
	var uname = getText(uel), uid = -1;
	var ind = uel.href.indexOf("edit=");
	if (ind !== -1)
		uid = parseInt(uel.href.substr(ind+5), 10);
	if (isNaN(uid) || uid < 0) {
		return null;
	}

	var user = {
		'name': uname,
		'id': uid
	};
	return user;
}

// Set username, id.
var meUser;
function lookForUser() {
	try {
		var d = frames[1].document;
		var feedLink = d.getElementById('todaypage-rss-feed-link');
		if (feedLink) {
			// The feed link exists, so so should the user info.
			var myUEl = getElementsByClassName(d, 'ctrl-button', 'a')[0];
			if (!myUEl)
				throw 0;
			meUser = getUserFromLink(myUEl);
			main();
			return;
		}
	}
	catch(e) {
		if (e === 0) throw new Error("User info not found.");
		// else document has not loaded.
	}

	// Keep spamming until the user info appears.
	setTimeout(lookForUser, 100);
}
lookForUser();

function main() {
	if (!meUser)
		throw new Error("Invalid user info.");

	// Only allow people on the white-list.
	if (!(meUser.id in userClass))
		throw -10;

	var topHead = document.getElementsByTagName('head')[0];

	document.title = "Fronter - det nya Facebook";

	// Determine the path everything resides under.
	function getPath(url) {
		var q = url.indexOf('?');
		if (q !== -1) url = url.substr(0, q);
		q = url.indexOf('#');
		if (q !== -1) url = url.substr(0, q);
		url = url.substr(url.indexOf('//') + 2);
		return url.substr(url.indexOf('/'));
	}
	var topPath = getPath(window.location.href);
	topPath = /^(.+)\/[^\/]*$/.exec(topPath);
	topPath = (topPath && topPath[1]) || '/stockholm';
	var arkivUrl = topPath + "/links/index2.phtml?v=6&orderid=2&toolprjid=459322984";

	// This fails for FF 3.5, which is incidentally the version of Firefox
	// our school has...
	var hasReadyState = !!document.readyState;

	var isIE = !!document.all;

	function hideName(d) {

		function prevel(el, type) {
			if (el === null) return null;
			do {
				el = el.previousSibling;
			}
			while (el !== null && el.nodeName !== type);
			return el;
		}

		// Hide the injection site, so you can't see my name as easily, and
		// remove links to fronter.js.
		if (!d.hasRem) {
			d.hasRem = true;
			var dds = d.getElementsByTagName('dd');
			for (var i = 0; i < dds.length; ++i) {
				var sps = dds[i].getElementsByTagName('a');
				if (sps.length === 0)
					continue;
				var html = sps[sps.length-1].innerHTML;
				if (html === 'i7qkdxo' || html === 'Aktivera det nya fronter') {
					var e1 = sps[0].parentNode, e2 = prevel(e1, 'DT');
					var par = e1.parentNode;
					par.removeChild(e1);
					par.removeChild(e2);
					dds = d.getElementsByTagName('dd');
					--i;
				}
			}
		}
	}

	// Don't provide support for substandard browsers.
	if (!window.localStorage) {
		throw new Error("Föråldrad webbläsare.");
	}


	/* Some useful functions */

	// Cross-browser event adding.
	function addListener(el, ev, handler) {
		if (el.addEventListener)
			el.addEventListener(ev, handler, false);
		else
			el.attachEvent('on'+ev, handler);
	}

	function getWin(doc) {
		return doc.defaultView || doc.parentWindow;
	}

	// Cross-browser stylesheet adding, taken from MDC.
	function addStylesheetRules (d, rules) {
		var style = d.createElement('style');
		d.getElementsByTagName('head')[0].appendChild(style);
		if (!window.createPopup) { // Safari
			style.appendChild(d.createTextNode(''));
		}
		var s = d.styleSheets[d.styleSheets.length - 1];
		for (var selector in rules) {
			try {
				var rule = rules[selector];
				if (rule.join) rule = rule.join(';');
				if (s.insertRule) {
						s.insertRule(selector + '{' + rule + '}', s.cssRules.length);
				}
				else { // IE
					s.addRule(selector, rule, -1);
				}
			} catch(e) {}
		}
	}

	// Array.indexOf polyfill, taken from MDC.
	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(searchElement /*, fromIndex */) {
			"use strict";

			if (this === void 0 || this === null)
				throw new TypeError();

			var t = Object(this);
			var len = t.length >>> 0;
			if (len === 0)
				return -1;

			var n = 0;
			if (arguments.length > 0) {
				n = Number(arguments[1]);
				if (n !== n) // shortcut for verifying if it's NaN
					n = 0;
				else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0))
					n = (n > 0 || -1) * Math.floor(Math.abs(n));
			}

			if (n >= len)
				return -1;

			var k = (n >= 0 ? n : Math.max(len - Math.abs(n), 0));

			for (; k < len; k++) {
				if (k in t && t[k] === searchElement)
					return k;
			}
			return -1;
		};
	}

	// ES6 String.startsWith polyfill.
	if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(a) {
			return this.lastIndexOf(a, 0) === 0;
		};
	}

	if (!window.console) window.console = {};
	if (!console.log) console.log = function() {};

	function hasClass(el, cl) {
		return (' ' + el.className + ' ').indexOf(' ' + cl + ' ') !== -1;
	}
	function trim(str) {
		return str.replace(/^\s+|\s+$/g,"");
	}
	function ltrim(str) {
		return str.replace(/^\s+/,"");
	}
	function rtrim(str) {
		return str.replace(/\s+$/,"");
	}

	function linkify(text) {
		function urlToLink(text) {
			var url;
			if (text.startsWith('http://') || text.startsWith('https://'))
				url = text;
			else
				url = 'http://' + text;
			var attrHTML = url.replace(/"/g, '&quot;');
			return '<a href="' + attrHTML + '" target="_blank">' + text + '</a>';
		}

		var rest = text.replace(/&/g, '&amp;').replace(/</g, '&lt;');
		var out = '';

		for (;;) {
			var pos = rest.search(/( |\(|^)(http:\/\/|https:\/\/|www\.)/m);
			if (pos < 0) {
				out += rest;
				break;
			}

			if (/[ (]/.test(rest.charAt(pos))) ++pos;
			out += rest.substr(0, pos);
			rest = rest.substr(pos);

			var end = rest.search(/$|,| |\.(\W|$)|\)(\.(\W|$)|[,!?)]|$)/m);
			if (/\?!/.test(rest.charAt(end-1))) --end;
			out += urlToLink(rest.substr(0, end));
			rest = rest.substr(end);
		}
		return out;
	}

	function addPlaceholder(el) {
		if ('placeholder' in el)
			return;
		var pl = el.getAttribute('placeholder');
		el.value = pl;
		el.className = 'hasPlaceholder';
		addListener(el, 'focus', function() {
			if (el.className === 'hasPlaceholder') {
				el.className = '';
				el.value = '';
			}
		});
		addListener(el, 'blur', function() {
			if (el.value === '') {
				el.className = 'hasPlaceholder';
				el.value = pl;
			}
		});
	}


	/* Facebook things */

	// Periodic updates
	var lastEvent = 0, firstEvents = true;
	var lastOnlineEvent = 0;
	var timeCorrection;
	var allEvents = [];

	var firstEventsCallback = null, incrementalEventsCallback = function() {};
	function getIncrementalEvents(callback) {
		incrementalEventsCallback = callback;
	}
	function getFirstEvents(callback) {
		if (firstEvents) {
			// Still waiting, set a callback instead.
			firstEventsCallback = callback;
		}
		else {
			callback();
		}
	}

	window.callbackGetEvents = function(after, serverTime, events, onlineList, newLastOnlineEvent) {
		if (firstEvents) {
			timeCorrection = new Date() - serverTime*1000;
		}

		var id = after;
		var allEventsIndex = allEvents.length;
		for (var i = 0; i < events.length; ++i) {
			// Process only events not already processed (we sometimes get
			// duplicate callbacks, for example when commenting and requesting
			// an update at the same time).
			if (++id <= lastEvent)
				continue;
			++lastEvent;

			// Skip deleted events.
			var ev = events[i];
			if (ev === null) continue;

			var type = ev[0];
			var jsEvent = {
				id: id,
				type: type,
				user: getUserFromId(ev[1]),
				timestamp: ev[ev.length-1]*1000 + timeCorrection
			};

			// Ignore invalid users.
			if (!jsEvent.user) continue;

			if (type === 0) { // comment
				jsEvent.parent = ev[2];
				jsEvent.text = ev[3];
			}
			else if (type === 1) { // entry
				var text = ev[2];
				jsEvent.toAll = (text.charAt(0) === '1');
				if (!jsEvent.toAll && !sameClass(jsEvent.user))
					continue;
				jsEvent.text = text.substr(1);
			}
			else if (type === 2) { // like
				jsEvent.parent = ev[2];
			}
			allEvents.push(jsEvent);
		}

		try {
			if (firstEvents) {
				firstEvents = false;
				if (firstEventsCallback) {
					firstEventsCallback();
					firstEventsCallback = null;
				}
			}
			else {
				incrementalEventsCallback(allEventsIndex);
			}
		}
		catch(e) {} // page unloaded

		if (onlineList) {
			lastOnlineEvent = newLastOnlineEvent;
			for (var i = 0; i < onlineList.length; ++i) {
				OnlineList.receive(onlineList[i]);
			}
		}
	};

	var sidebarLoaded = false;
	var periodicUpdateTimer = null;
	function timePeriodicUpdate() {
		if (periodicUpdateTimer !== null)
			clearTimeout(periodicUpdateTimer);
		periodicUpdateTimer = setTimeout(function() {
			periodicUpdateTimer = null;
			periodicUpdate();
		}, 10000);
	}
	function getPeriodicUpdateParams() {
		var ret = "after=" + lastEvent;
		if (sidebarLoaded) {
			// We are connected to the chat - send online info.
			ret += "&o=" + meUser.id + ',' + lastOnlineEvent;
		}
		return ret;
	}
	function fetchInitialChatUsers() {
		if (firstEvents) {
			// The initial update is currently being fetched; send a simultaneous
			// request with a high lastEvent to make only o= relevant.
			lastEvent = 1000000000;
			periodicUpdate();
			lastEvent = 0;
		}
		else {
			periodicUpdate();
		}
	}
	function periodicUpdate() {
		var sc = document.createElement('script');
		var url = "http://localhost/getevents.php?" + getPeriodicUpdateParams();
		sc.src = url;
		sc.charset = 'utf-8';
		topHead.appendChild(sc);
		timePeriodicUpdate();

		setTimeout(function() {
			//Store browser statistics
			var sc = document.createElement('script');
			sc.src = "http://localhost/log.php?id=" + meUser.id;
			sc.charset = 'utf-8';
			topHead.appendChild(sc);
		}, 1000);
	}
	periodicUpdate();

	function getPrettyYearDay(d, m, y) {
		var months = ["januari", "februari", "mars", "april", "maj", "juni",
			"juli", "augusti", "september", "oktober", "november", "december"];
		var date = "den " + d + " " + months[m-1];
		if (y !== 2012)
			date += " " + y;
		return date;
	}

	function getPrettyTime(timestamp) {
		var ago = (new Date() - timestamp);
		if (ago < 60*1000) return "några sekunder sedan";
		if (ago < 2*60*1000) return "för ungefär en minut sedan";
		if (ago < 60*60*1000) return "för " + Math.floor(ago/60/1000) + " minuter sedan";
		if (ago < 2*60*60*1000) return "för ungefär en timme sedan";
		if (ago < 24*60*60*1000) return "för " + Math.floor(ago/60/60/1000) + " timmar sedan";

		var t = new Date(timestamp);
		var date = t.getDate();
		var hour = t.getHours()+'';
		var min = t.getMinutes()+'';
		if (hour.length < 2) hour = '0' + hour;
		if (min.length < 2) min = '0' + min;
		var end = " kl. " + hour + ":" + min;

		// Check for recent days.
		if (ago < 6*24*60*60*1000) {
			// The day is at most a week ago, so we only have to check dates for equality.
			var day = new Date(), counter = 0;
			while (counter < 4) {
				if (day.getDate() === date) {
					var weekdays = ["söndag", "måndag", "tisdag", "onsdag", "torsdag", "fredag", "lördag"];
					if (counter === 0) return "Idag" + end;
					if (counter === 1) return "Igår" + end;
					return weekdays[day.getDay()] + end;
				}
				day.setDate(day.getDate()-1);
				++counter;
			}
		}

		return getPrettyYearDay(date, t.getMonth()+1, t.getFullYear()) + end;
	}

	function updatingTimestamp(el, time) {
		var win = getWin(el.ownerDocument);
		function update() {
			setText(el, getPrettyTime(time));

			var ago = (new Date() - time), next;
			if (ago < 60*1000) next = 100 + 60*1000 - ago;
			else if (ago < 60*60*1000) next = 100 + 60*1000 - (ago % (60*1000));
			else if (ago < 24*60*60*1000) next = 100 + 60*60*1000 - (ago % (60*60*1000));
			else {
				// Find the start of the next day.
				var d = new Date();
				d.setDate(d.getDate()+1);
				d.setHours(0);
				d.setMinutes(0);
				d.setSeconds(0);
				next = d - time;
			}
			win.setTimeout(update, next);
		}
		update();
	}
    
	function sendLike(id) {
		var sc = document.createElement('script');
		var url = "http://localhost/storelike.php";
		url += "?id=" + id;
		url += "&user=" + meUser.id;
		url += "&" + getPeriodicUpdateParams();
		sc.src = url;
		sc.charset = 'utf-8';
		topHead.appendChild(sc);
		timePeriodicUpdate();
	}

	function updateLike(d, ev) {
		var id = ev.parent, user = ev.user;
		var obj = d.likeObjects[id];
		if (!obj) {
			try {
				if (!d.unusedLikes[id])
					d.unusedLikes[id] = [];
				d.unusedLikes[id].push(ev);
			}
			catch(e) {}
			return;
		}
		if (user.id === meUser.id) {
			try {
				if (localStorage['like' + id] === '2')
					return;
			}
			catch(e) {}
			obj.actually();
		}
		else {
			obj.addLike(user);
		}
		obj.updateText();
	}

	function setLikeBtn(d, btn, id, likeObj) {
		var likeStatus = 0;
		function update() {
			setText(btn, (likeStatus === 1 ? "Sluta gilla" : "Gilla"));
		}
		function setLike(v) {
			likeStatus = v;
			likeObj.ilike = (likeStatus === 1);
			try {
				localStorage['like' + id] = likeStatus;
			}
			catch(e) {}
			update();
		}

		try {
			likeStatus = +localStorage['like' + id] || 0;
		}
		catch(e) {}
		update();
		likeObj.ilike = (likeStatus === 1);

		var timer = null;
		btn.onclick = function() {
			if (likeStatus === 0) {
				sendLike(id);
			}
			else {
				if (timer !== null)
					clearTimeout(timer);
				timer = setTimeout(function() {
					likeObj.updateText();
				}, 300);
			}
			setLike(likeStatus === 1 ? 2 : 1);
		};

		likeObj.actually = function() {
			// It turns out that we like this.
			likeObj.ilike = true;
			if (likeStatus === 0)
				setLike(1);
		};

		d.likeObjects[id] = likeObj;
	}

	function sendEntry(text, toAll) {
		text = (toAll ? '1' : '0') + text;
		var sc = document.createElement('script');
		var url = "http://localhost/storeentry.php";
		url += "?user=" + meUser.id;
		url += "&text=" + encodeURIComponent(text);
		url += "&" + getPeriodicUpdateParams();
		sc.src = url;
		sc.charset = 'utf-8';
		topHead.appendChild(sc);
		timePeriodicUpdate();
	}

	// News items
	function News(d, id, user, text, date, container, external, toAll) {
		this.d = d;
		this.id = id;
		this.user = user;
		this.shortText = text;
		this.toAll = toAll;
		if (external) {
			if (/^\d\d\d\d-\d\d-\d\d$/.test(date)) {
				var dateParts = date.split('-');
				date = getPrettyYearDay(+dateParts[2], +dateParts[1], +dateParts[0]);
			}
			this.prettyDate = date;
		}
		else {
			this.timestamp = date;
		}
		this.addToDOM(container, external);
	}

	News.convertExisting = function(d, item) {
		var header = getElementsByClassName(item, 'news-content-header')[0];
		if (!header)
			return null;
		var htext = getText(header);

		var newsId, ind;
		var readMoreLink = header.parentNode.getElementsByTagName('a')[0];
		if (readMoreLink) {
			var href = readMoreLink.href;
			ind = href.indexOf('news_id%3D');
			if (ind !== -1) {
				ind += 'news_id%3D'.length;
				newsId = +href.substring(ind, href.indexOf('%', ind));
			}
		}
		if (isNaN(newsId))
			return null;

		var contentDesc = getElementsByClassName(header.parentNode, 'content-desc')[0];
		if (!contentDesc)
			return null;

		var nameLink = contentDesc.getElementsByTagName('a')[0];
		var user = nameLink && getUserFromLink(nameLink);
		if (!user)
			return null;

		var date = getText(contentDesc);
		ind = date.lastIndexOf(' ');
		date = date.substr(ind+1);

		var cont = item.parentNode;
		while (cont.nodeName !== 'TBODY') {
			cont = cont.parentNode;
		}
		d.newsContainer = cont;
		return new News(d, newsId, user, htext, date, item, true, true);
	};

	News.prototype.loadExternalContents = function() {
		var req = new XMLHttpRequest();
		req.open("GET", "http://fronter.com/stockholm/news/?action=read_news&news_id=" + this.id, true);
		var self = this;
		function done(text) {
			var ind = text.indexOf('<font');
			if (ind === -1) throw 0;
			ind = text.indexOf('<br><br>', ind);
			if (ind === -1) throw 0;
			ind += 8;
			var end = text.lastIndexOf('right-align');
			end = text.lastIndexOf('</table>', end);
			end = text.lastIndexOf('</td>', end);
			var html = text.substring(ind, end);

			self.contents.innerHTML = self.shortText + '<br><br>' + html;
			self.moreBtn.parentNode.removeChild(self.moreBtn);
		}
		req.onreadystatechange = function() {
			if (req.readyState === 4) {
				try {
					if (req.status === 200) {
						done(req.responseText);
						return;
					}
				}
				catch(e) {}
				// Fail?
			}
		};
		req.send(null);
	};

	News.prototype.sendComment = function(text) {
		var sc = document.createElement('script');
		var url = "http://localhost/storecomment.php";
		url += "?id=" + this.id;
		url += "&user=" + meUser.id;
		url += "&text=" + encodeURIComponent(text);
		url += "&" + getPeriodicUpdateParams();
		sc.src = url;
		sc.charset = 'utf-8';
		topHead.appendChild(sc);
		timePeriodicUpdate();
	};
	

	News.addBefore = function(d, before, id, user, text, date, toAll) {
		var row = d.createElement('tr');
		var item = d.createElement('td');
		row.appendChild(item);
		d.newsContainer.insertBefore(row, before);
		return new News(d, id, user, text, date, item, false, toAll);
	};

	News.prototype.openActionBox = function() {
		this.actionBox.style.display = 'block';
	};

	News.prototype.addToCommentCounter = function(n) {
		this.commentCounter += n;
		setText(this.acViewAllCounter, this.commentCounter);
	};

	News.prototype.addInitialComments = function(list) {
		for (var i = Math.max(0, list.length-2); i < list.length; ++i) {
			this.addComment(list[i]);
		}
		if (list.length > 2) {
			this.acViewAll.style.display = 'block';
			this.addToCommentCounter(list.length-2);
			var self = this;
			this.acViewAll.lastChild.onclick = function() {
				this.onclick = null;
				setTimeout(function() {
					self.acViewAll.style.display = 'none';
					for (var i = list.length-2; i --> 0; ) {
						self.addComment(list[i], true);
						try {
							var d = self.d;
							var likes = d.unusedLikes[list[i].id];
							if (likes && likes.length) {
								for (var j = 0; j < likes.length; ++j) {
									updateLike(d, likes[j]);
								}
								delete d.unusedLikes[list[i].id];
							}
						}
						catch(e) {}
					}
				}, 400);
			};
		}
	};

	News.prototype.addComment = function(comment, front) {
		this.openActionBox();
		var ul = this.acComments.firstChild;
		var d = this.d;

		var imageUrl = getUserAvatar(comment.user);
		var userUrl = getUserLink(comment.user);
		var el = d.createElement('li');
		el.className = 'acItem acComment';
		el.innerHTML =
			"<img src='" + imageUrl + "'>" +
			"<div class='acComment-contents'>" +
				"<a href='" + userUrl + "' class='main-name'>" + comment.user.name + "</a> " +
				"<span></span>" +
				"<p class='acComment-info'>" +
					"<span></span>" +
					" &middot; " +
					"<a class='fakelink likebtn'></a>" +
					"<span class='liketext'>" +
						" &middot; " +
						"<i></i>" +
						"<span></span>" +
					"</span>" +
				"</p>" +
			"</div>";

		var msgCont = el.getElementsByTagName('span')[0];
		msgCont.innerHTML = linkify(comment.text);

		var timestampText = el.getElementsByTagName('span')[1];
		updatingTimestamp(timestampText, comment.timestamp);

		var likeBtn = el.getElementsByClassName('likebtn')[0];
		var likeText = likeBtn.nextSibling;
		var likeObj = {ilike: false, likes: []};
		likeObj.updateText = function() {
			var nlikes = likeObj.likes.length + (likeObj.ilike ? 1 : 0);
			if (nlikes === 0) {
				likeText.style.display = 'none';
				return;
			}
			likeText.style.display = 'inline';
			setText(likeText.lastChild, nlikes);
		};
		likeObj.addLike = function(user) {
			likeObj.likes.push(user.name);
		};
		setLikeBtn(d, likeBtn, comment.id, likeObj);
		likeText.lastChild.onclick = function() {
			var text = likeObj.likes.join('\n');
			if (likeObj.ilike) {
				if (text) text = '\n' + text;
				text = meUser.name + text;
			}
			alert(text);
		};

		if (front)
			ul.insertBefore(el, ul.firstChild);
		else
			ul.appendChild(el);
		this.addToCommentCounter(1);
	};

	News.prototype.addToDOM = function(item, external) {
		var imageUrl = getUserAvatar(this.user);
		var userUrl = getUserLink(this.user);

		var d = this.d;
		item.innerHTML =
			"<div class='newsitem'>" +
				"<img src='" + imageUrl + "' alt='Image'>" +
				"<div class='newsiteminfo'>" +
					"<a href='" + userUrl + "' class='main-name'>" + this.user.name + "</a>" +
					"<div class='news-contents'></div>" +
					(external ?
						"<p style='margin-top: 1px;'><a class='morebtn'>Visa mer</a></p>" :
						"") +
					"<p style='margin-top: 3px;'>" +
						"<span class='faded'>" +
							"<a class='fakelink likebtn'></a>" +
							" &middot; " +
							"<a class='fakelink commentbtn'>Kommentera</a>" +
							" &middot; " +
							"<span class='timestamp'></span>" +
							(this.toAll ?
								"" :
								" &middot; <i class='icon onlyclass' title='Bara klassen ser detta'></i>") +
						"</span>" +
					"</p>" +
					"<ul class='actions'>" +
						"<li class='acBorder'><i></i></li>" +
						"<li class='acItem acLikes'><i></i><span><a>4 personer</a> gillar detta.</span></li>" +
						"<li class='acItem acViewAll'><i></i><a>Visa alla <span></span> kommentarer</a></li>" +
						"<li class='acComments'>" +
							"<ul></ul>" +
						"</li>" +
						"<li class='acItem acWrite'>" +
							"<img src='http://fronter.com/stockholm/contacts/myimage.phtml?view=" + meUser.id + ".gif'>" +
							"<span class='input-wrapper'>" +
								"<input placeholder='Kommentera...' maxlength='90'>" +
							"</span>" +
						"</li>" +
					"</ul>" +
				"</div>" +
			"</div>" +
			"<hr class='news-separator'>";

		this.contents = getElementsByClassName(item, 'news-contents')[0];
		if (external) {
			setText(this.contents, this.shortText);
		}
		else {
			this.contents.innerHTML = linkify(this.shortText);
			this.contents.style.whiteSpace = 'pre-line';
		}
		this.moreBtn = getElementsByClassName(item, 'morebtn')[0];
		this.likeBtn = getElementsByClassName(item, 'likebtn')[0];
		this.commentBtn = getElementsByClassName(item, 'commentbtn')[0];

		var timestampText = getElementsByClassName(item, 'timestamp')[0];
		if (external) {
			setText(timestampText, this.prettyDate);
		}
		else {
			updatingTimestamp(timestampText, this.timestamp);
		}

		this.actionBox = getElementsByClassName(item, 'actions')[0];
		this.acLikes = getElementsByClassName(this.actionBox, 'acLikes')[0];
		this.acViewAll = getElementsByClassName(this.actionBox, 'acViewAll')[0];
		this.acComments = getElementsByClassName(this.actionBox, 'acComments')[0];
		this.acWrite = getElementsByClassName(this.actionBox, 'acWrite')[0];

		this.acViewAllCounter = this.acViewAll.getElementsByTagName('span')[0];
		this.commentCounter = 0;

		this.acLikes.style.display = 'none';
		this.acViewAll.style.display = 'none';
		this.actionBox.style.display = 'none';

		var self = this;
		if (external) {
			this.moreBtn.onclick = function() {
				self.loadExternalContents();
			};
		}

		var commentInp = this.acWrite.lastChild.firstChild;
		this.commentBtn.onclick = function() {
			self.openActionBox();
			commentInp.focus();
		};
		addPlaceholder(commentInp);

		var likeObj = {ilike: false, flikes: [], olikes: []};
		likeObj.updateText = function() {
			var o = likeObj;
			var any = (o.ilike || o.flikes.length || o.olikes.length);
			if (!any) {
				self.acLikes.style.display = 'none';
				return;
			}
			self.openActionBox();
			self.acLikes.style.display = 'block';
			var who = [];
			if (o.ilike)
				who.push("Du");
			for (var i = 0; i < 2 && i < o.flikes.length; ++i)
				who.push(getUserLinkHTML(o.flikes[i]));
			var rest = Math.max(o.flikes.length-2, 0) + o.olikes.length;
			if (rest) {
				any = (o.ilike || o.flikes.length);
				var oth = (rest === 1
						? (any ? " annan person" : " person")
						: (any ? " andra" : " personer"));
				who.push("<a class='others'>" + rest + oth + "</a>");
			}
			var html = who.slice(0, who.length-1).join(", ");
			if (html) html += " och";
			html += " " + who[who.length-1] + " gillar detta.";
			self.acLikes.lastChild.innerHTML = html;
			try {
				var links = self.acLikes.lastChild.getElementsByTagName('a');
				var others = links[links.length-1];
				if (others && others.className === 'others') {
					others.onclick = function() {
						var people = o.flikes.slice(2).concat(o.olikes);
						for (var i = 0; i < people.length; ++i) {
							people[i] = people[i].name;
						}
						alert(people.join('\n'));
					};
				}
			}
			catch(e) {} // defense
		};
		likeObj.addLike = function(user) {
			if (sameClass(user))
				likeObj.flikes.push(user);
			else
				likeObj.olikes.push(user);
		};
		setLikeBtn(d, this.likeBtn, this.id, likeObj);

		commentInp.onkeydown = function(e) {
			var v = trim(this.value);
			if (!e) {
				var win = getWin(this.ownerDocument);
				if (win.event) e = win.event;
				else e = window.event;
			}
			if (e.keyCode === 13 && v) {
				if (limitSpam(1, 10))
					return false;
				self.sendComment(v);
				this.value = '';
			}
		};

		var commentAvatar = this.acWrite.firstChild;
		commentAvatar.style.display = 'none';
		commentInp.onfocus = function() {
			this.parentNode.style.marginLeft = '36px';
			commentAvatar.style.display = 'block';
			self.acWrite.style.minHeight = '32px';
		};
		commentInp.onblur = function() {
			this.parentNode.style.marginLeft = '';
			commentAvatar.style.display = 'none';
			self.acWrite.style.minHeight = '';
		};
	};

	function scoreEntry(entry) {
		var s = 200;
		if (sameClass(entry.user))
			s += 30;
		s += entry.nLikes*4;
		s += entry.nComments*6;
		if (!entry.nComments && !entry.nLikes)
			s -= 15;
		s *= Math.pow(0.35, (new Date() - entry.timestamp) / (24*60*60*1000));
		return s;
	}
	function compareEntries(a, b) {
		return scoreEntry(b) - scoreEntry(a);
	}


	// Online list
	var OnlineList = {
		map: {},
		receive: function(data) {
			var id = data[0], online = !!data[1];
			var now = new Date()-0;
			if (online && !this.map[id])
				this.add(id);
			else if (!online && this.map[id])
				this.remove(id);
		},
		add: function(id) {
			this.map[id] = 1;
			pageSidebarAddOnline(true, id);
		},
		remove: function(id) {
			delete this.map[id];
			pageSidebarAddOnline(false, id);
		},
		sendAll: function() {
			for (var id in this.map)
				pageSidebarAddOnline(true, +id);
		}
	};

	var lastMessageTimes = [0, 0];
	function limitSpam(ind, sec) {
		var lmt = lastMessageTimes[ind];
		if (!lmt) {
			try {
				lmt = +localStorage['frbook-lastmessagetime'+ind] || 0;
				if (lmt) lastMessageTimes[ind] = lmt;
			}
			catch(e) {}
		}

		// Limit spam rates.
		var timeNow = +new Date();
		var waitTimeLeft = lmt + sec*1000 - timeNow;
		if (waitTimeLeft > 0) {
			alert("Vänligen vänta " +
				Math.ceil(waitTimeLeft / 1000) +
				" sekunder innan du kan skicka ett till meddelande.");
			return true;
		}

		lastMessageTimes[ind] = timeNow;
		try {
			localStorage['frbook-lastmessagetime'+ind] = timeNow;
		}
		catch(e) {}

		return false;
	}


	// The chat sidebar

	// Callbacks to be set later.
	var pageSidebarSetLoaded = function() {};
	var pageSidebarAddOnline = function() {};
	var pageSidebarReceive = function() {};
	var doSendChat = function() {};

	function initPubnub(w1) {
		// Initialize PUBNUB in a hidden iframe, so it gets a window load
		// event (which it depends on).
		var ifr = w1.document.createElement('iframe');
		ifr.style.position = 'absolute';
		ifr.style.top = '-100px';
		ifr.style.left = '-100px';
		ifr.style.height = '1px';
		ifr.style.width = '1px';
		w1.document.body.appendChild(ifr);

		var hasLoaded = false;
		function doLoad(pubnub) {
			if (hasLoaded)
				return;
			hasLoaded = true;
			console.log("Has PUBNUB!");

			var p = pubnub.init({
				subscribe_key: 'sub-c998fdd7-769f-11e1-9498-394bb371268f',
				publish_key: 'pub-aa8eadaa-960a-4d06-aff2-e974a7c37b10'
			});
			p.subscribe({
				channel: 'user' + meUser.id,
				callback: function(data) {
					pageSidebarReceive(data);
				},
				connect: function() {
					console.log("Connected to chat!");
					setupSidebar(true);
				},
				error: function(e) {
					console.log("PUBNUB subscribe error: ", e);
					setupSidebar(false, "Fel?");
				}
			});
			doSendChat = function(to, data, callback) {
				p.publish({
					channel: 'user' + to.id,
					message: data,
					callback: function(info) {
						if (info[0] === 1) {
							// Success
							callback();
						}
						else {
							console.log("PUBNUB publish error: ", info[1]);
						}
					}
				});
			};
		}
		var w = w1.frames[0];
		w.onload = function() {
			if ("PUBNUB" in w)
				doLoad(w.PUBNUB);
		};
		w.document.open();
		w.document.write(
			"<script src=http://cdn.pubnub.com/pubnub-3.1.min.js></script>"
		);
		w.document.close();

		var tries = 20;
		function tryLoad() {
			if ("PUBNUB" in w)
				doLoad(w.PUBNUB);
			else if (--tries)
				setTimeout(tryLoad, 100);
		}
		setTimeout(tryLoad, 100);
	}

	function setupSidebar(success, e) {
		if (sidebarLoaded)
			return;
		sidebarLoaded = success;
		pageSidebarSetLoaded(sidebarLoaded, e);
		if (sidebarLoaded)
			fetchInitialChatUsers();
	}
	function destroyMainPage() {
		pageSidebarSetLoaded = function() {};
		pageSidebarAddOnline = function() {};
		pageSidebarReceive = function() {};
		firstEventsCallback = null;
		incrementalEventsCallback = function() {};
	}

	var titleBlinkTimer = null;
	function setChatTitle(user) {
		if (user) {
			var firstName = user.name;
			var ind = firstName.indexOf(' ');
			if (ind !== -1)
				firstName = firstName.substr(0, ind);
			document.title = firstName + " har skickat ett meddelande till dig";
		}
		else {
			document.title = "Fronter - det nya Facebook";
		}
	}
	function markIncomingChat(user) {
		if (titleBlinkTimer === null) {
			var c = 0;
			titleBlinkTimer = setInterval(function() {
				setChatTitle((++c)%2 ? user : null);
			}, 1000);
		}

		try {
			var snd = null;
			if (new Audio().canPlayType("audio/ogg; codecs=vorbis"))
				snd = new Audio("http://static.ak.fbcdn.net/sound/pling.ogg");
			else
				snd = new Audio("http://static.ak.fbcdn.net/sound/pling.mp3");
			snd.play();
		}
		catch(e) {
			try {
				var el = frames[0].document.getElementById('bottom-row');
				el.style.display = 'block';
				el.style.top = '-300px';
				el.innerHTML =
					"<embed src='" + 
					"http://static.ak.fbcdn.net/sound/pling.mp3" +
					"' hidden=true autostart=true loop=false>";
			}
			catch(e) {} // whatever
		}
	}
	function tabFocused() {
		if (titleBlinkTimer !== null) {
			clearInterval(titleBlinkTimer);
			titleBlinkTimer = null;
			setChatTitle(null);
		}
	}
	window.onfocus = tabFocused;

	function ChatSidebar(d) {
		var sidebar = d.createElement('div');
		sidebar.id = 'chat-sidebar';
		sidebar.innerHTML =
			"<span id='chat-sidebar-error'></span>" +
			"<div id='chat-sidebar-scroller'>" +
				"<div id='chat-sidebar-contents'>" +
					"<ul id='chat-sidebar-list'>" +
					"</ul>" +
				"</div>" +
			"</div>";
		d.body.appendChild(sidebar);

		this.chatsEl = d.createElement('div');
		this.chatsEl.id = 'chats';
		d.body.appendChild(this.chatsEl);

		this.d = d;
		this.el = sidebar;
		this.items = {};
		this.chats = {};
		this.nchats = 0;
		this.list = d.getElementById('chat-sidebar-list');

		var self = this;
		pageSidebarSetLoaded = function(loaded, e) {
			try {
				self.setLoaded(loaded, e);
			} catch(e) {} // page discarded
		};
		pageSidebarAddOnline = function(online, id) {
			try {
				var user = getUserFromId(id);
				if (user)
					self.addOnline(online, user);
			}
			catch(e) {} // page discarded
		};
		pageSidebarReceive = function(data) {
			try {
				var uid = data.user, text = data.message;
				var user = {id: uid, name: userNames[uid]};
				self.receiveLine(user, text);
			}
			catch(e) {} // page discarded
		};

		window.addSampleUsers = function() {
			self.addOnline(true, {id: 697051438, name: "Dan Lidholm"});
			self.addOnline(true, {id: 531757658, name: "Simon Lindholm"});
			self.addOnline(true, {id: 761198024, name: "Henrik Hellström"});
		};
		if (sidebarLoaded)
			pageSidebarSetLoaded(true);
		else
			pageSidebarSetLoaded(false, "Laddar...");
		OnlineList.sendAll();
	}

	ChatSidebar.prototype.setLoaded = function(loaded, e) {
		var scroller = this.d.getElementById('chat-sidebar-scroller');
		scroller.style.display = (loaded ? '' : 'none');
		var errorText = this.d.getElementById('chat-sidebar-error');
		errorText.style.display = (loaded ? 'none' : '');
		if (!loaded)
			setText(errorText, e);
	};

	ChatSidebar.prototype.focusChat = function(user) {
		this.openChat(user);
		var inp = this.chats[user.id].getElementsByTagName('input')[0];
		inp.focus();
	};

	ChatSidebar.prototype.openChat = function(user) {
		if (this.chats[user.id])
			return;
		if (this.nchats === 3) {
			// Close the first chat (four is too many)
			for (var c in this.chats) {
				this.closeChat(+c);
				break;
			}
		}
		var ch = this.d.createElement('div');
		ch.className = 'chat';
		ch.innerHTML =
			"<div class='chat-title'>" +
				"<span>" + user.name + "</span>" +
				"<a class='chat-title-close'></a>" +
			"</div>" +
			"<div class='chat-content'></div>" +
			"<div class='chat-field'>" +
				"<i></i>" +
				"<div class='input-wrapper'>" +
					"<input type='text'>" +
				"</div>" +
			"</div>";
		this.chatsEl.appendChild(ch);

		var closeBtn = getElementsByClassName(ch, 'chat-title-close')[0];
		var self = this;
		closeBtn.onclick = function() {
			self.closeChat(user.id);
		};

		var content = getElementsByClassName(ch, 'chat-content')[0];
		content.onclick = function() {
			self.focusChat(user);
		};

		var inp = ch.getElementsByTagName('input')[0];
		inp.onkeydown = function(e) {
			var c = trim(this.value);
			if (!e) {
				var win = getWin(this.ownerDocument);
				if (win.event) e = win.event;
				else e = window.event;
			}
			if (e.keyCode === 13 && c && this.className !== 'hasPlaceholder') {
				self.sendChat(user, c, function() {
					self.addLine(user, meUser, c);
				});
				this.value = '';
			}
		};

		++this.nchats;
		this.chats[user.id] = ch;
		this.chatsEl.style.display = 'block';
	};
	ChatSidebar.prototype.closeChat = function(uid) {
		--this.nchats;
		var ch = this.chats[uid];
		this.chatsEl.removeChild(ch);
		delete this.chats[uid];
		if (this.nchats === 0) {
			this.chatsEl.style.display = 'none';
		}
	};

	ChatSidebar.prototype.sendChat = function(to, text, callback) {
		doSendChat(to, {user: meUser.id, message: text}, callback);
	};

	ChatSidebar.prototype.receiveLine = function(user, text) {
		try {
			if (!document.hasFocus()) {
				markIncomingChat(user);
			}
		}
		catch(e) {} // not supported
		this.openChat(user);
		this.addLine(user, user, text);
	};
	ChatSidebar.prototype.addLine = function(chatUser, user, text) {
		var ch = this.chats[chatUser.id];
		var content = getElementsByClassName(ch, 'chat-content')[0], line;
		var atBottom = (content.scrollTop + content.offsetHeight === content.scrollHeight);

		if (content.lastChild && content.lastChild.user.id === user.id) {
			line = content.lastChild;
		}
		else {
			line = this.d.createElement('div');
			var imageUrl = getUserAvatar(user);
			line.className = 'chat-line';
			line.innerHTML =
				"<img src='" + imageUrl + "' alt=''>" +
				"<div></div>";
			line.user = user;
			content.appendChild(line);
		}

		var p = this.d.createElement('p');
		var lcont = line.getElementsByTagName('div')[0];
		p.innerHTML = linkify(text);
		lcont.appendChild(p);

		if (atBottom) {
			content.scrollTop = content.scrollHeight;
		}
	};

	ChatSidebar.prototype.addOnline = function(online, user) {
		if (online) {
			if (this.items[user.id]) {
				console.error("User already online.");
				return;
			}
			var item = this.d.createElement('li');
			item.innerHTML =
				"<a>" +
					"<div class='avatar'>" +
						"<img alt='' src='" + getUserAvatar(user) + "'>" +
					"</div>" +
					"<span class='name'>" + user.name + "</span>" +
				"</a>";
			var link = item.firstChild;
			var self = this;
			link.onclick = function() {
				self.focusChat(user);
			};
			this.list.appendChild(item);
			this.items[user.id] = item;
		}
		else {
			var el = this.items[user.id];
			this.list.removeChild(el);
			delete this.items[user.id];
		}
	};

	// The header script, a sub-case of injCont.
	function injHeader(d) {
		addStylesheetRules(d, {
			'#customer-logo': 'display: none',
			'body': [
				'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
				'background-color: #3B5998'
			],
			'td': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'div': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'#top-row': 'background-image: none !important',
			'#bottom-row': 'background-image: none !important',
			'#tool-menu-links > li': 'background-color: #3B5998 !important',
			'li.open-menu-button:hover': 'background-image: url("http://images.fronter.com/volY11_images/Adult/topframe/slices/open_dropdown_button_bg.png") !important',
			'#search-box': 'display: none',
			'.dropdown-menu': [
				'border-bottom-color: #345978 !important',
				'border-top-color: #0978AF !important',
				'background-image: none !important',
				'background-color: #243462 !important'
			]
		});

		try {
			var ptools = d.getElementById('personal-tools-list')
				.getElementsByTagName('li');
			for (var i = 0; i < ptools.length; ++i) {
				if (getText(ptools[i]) === "Mitt arkiv") {
					arkivUrl = ptools[i].firstChild.href;
					break;
				}
			}
		}
		catch(e) {}

		initPubnub(getWin(d));

		setTimeout(function() {
			// Add a delay before changing the header to more resemble Facebook,
			// to give the impression of Fronter being transformed.
			addStylesheetRules(d, {
				'#top-row': [
					'height: 37px !important',
					'margin: 0 auto',
					'max-width: 976px',
					'padding-right: 170px'
				],
				'#top-row a': [
					'font-size: 12px !important',
					'font-weight: bold !important',
					'color: #d8dfea !important',
					'padding: 0 !important'
				],
				'#top-frame': [
					'min-height: 0 !important',
					'background-color: #3B5998 !important',
					'border-bottom: 1px solid #133783'
				],
				'#bottom-row': 'display: none',
				'#search-and-account': 'display: none',
				'#search-1': [
					'border: 1px solid #294A8F',
					'border-top-color: #123682'
				],
				'#search-2': [
					'position: relative',
					'border-top: 1px solid #D9D9D9',
					'display: inline-block',
					'padding: 1px 3px 4px',
					'background-color: white'
				],
				'#search-field': [
					'-webkit-appearance: none',
					'border: 0 none',
					'outline: none',
					'font-size: 11px !important',
					'color: #1A1A1A',
					'width: 315px'
				],
				'#search-field::-webkit-input-placeholder': 'color: #777',
				'#search-field:-moz-placeholder': 'color: #777',
				'#search-field.hasPlaceholder': 'color: #777',
				'#search-icon': [
					'display: block',
					'position: absolute',
					'top: 0',
					'right: 0',
					'background-color: white',
					'background-image: url("http://simonsoftware.se/other/fronter/book/fb-sprite2.png")',
					'background-position: -81px 0',
					'background-repeat: no-repeat',
					'border: 0 none',
					'padding: 0',
					'cursor: pointer',
					'height: 19px',
					'width: 22px'
				]
			});

			var topRow = d.getElementById('top-row');

			var homeUrl = "personal/index.phtml";
			var allRoomsUrl = "/stockholm/adm/projects.phtml?mode=displayRoomchooser&amp;mo=1";
			var logoutUrl = "index.phtml?logout=1";
			topRow.innerHTML =
				"<div style='float: left; margin-top: 4px;'>" +
					"<a href='" + homeUrl + "' target='contentframe'>" +
						"<img src='http://simonsoftware.se/other/fronter/book/header.png' alt='fronter'>" +
					"</a>" +
				"</div>" +
				"<div style='float: left; margin-top: 11px;'>" +
					"<a href='" + allRoomsUrl + "' target='contentframe'>Grupper</a>" +
				"</div>" +
				"<div style='position: absolute; top: 10px; left: 184px;'>" +
					"<form action='/stockholm/prjframe/search_page.phtml' method='get' target='contentframe' id='search-form'>" +
						"<span id='search-1'><span id='search-2'>" +
							"<input id='search-field' name='content_str' autocomplete='off' placeholder='Sök'>" +
							"<button id='search-icon' type='submit' title='Sök'></button>" +
						"</span></span>" +
					"</form>" +
				"</div>" +
				"<div style='float: right; margin-top: 10px;'>" +
					"<a href='" + logoutUrl + "' target='_top'>Logga ut</a>" +
				"</div>";

			var searchField = d.getElementById('search-field');
			addPlaceholder(searchField);
			d.getElementById('search-form').onsubmit = function() {
				if (searchField.value === '' || searchField.className === 'hasPlaceholder')
					return false;
				setTimeout(function() {
					searchField.value = '';

					// Blur the search field, and update the placeholder.
					searchField.focus();
					searchField.blur();
				});
				return true;
			};

			// Set the header frame size to 38px, and lock it at that.
			var frameset = window.document.body;
			frameset.rows = '38px,*';
			try {
				if (frameset.__defineSetter__)
					frameset.__defineSetter__('rows', function() {});
				else if (Object.defineProperty) {
					Object.defineProperty(frameset, 'rows',
							{value: '38px,*', writable: false});
				}
			}
			catch(e) {}
		}, 300);
	}


	function injTop() {

		function getContDoc(el) {
			return el.contentDocument || (el.contentWindow ? el.contentWindow.document : null);
		}

		var hasFCL = false;
		function setInjFr(el) {
			el.onload = doInj;
			startDCLSpam(el, null);
		}
		function startDCLSpam(el, origDoc) {
			if (hasFCL) return;
			var tries = 1;
			function spam() {
				try {
					var d = getContDoc(el), rd = (d && d.readyState);
					if (d && rd !== 'uninitialized' && d !== origDoc) {
						if (rd === 'complete' || rd === 'interactive') {
							doInj.call(el);
						}
						else if (isIE) {
							// There is no DOMContentLoaded support, so just keep spamming.
							if (tries++ < 20) {
								setTimeout(spam, 40 + 10*tries);
							}
							else {
								// It hasn't loaded for 3 seconds, just give up.
								doInj.call(el);
							}
						}
						else {
							addListener(d, 'DOMContentLoaded', function() { doInj.call(el); });

							if (!hasReadyState) {
								// Guess, to make things kindof-work on school computers.
								setTimeout(function() {
									doInj.call(el);
								}, 500);
							}
						}
					}
					else {
						if (tries++ >= 10) return;
						setTimeout(spam, 50);
					}
				}
				catch(e) {} // domain errors
			}
			setTimeout(spam, 30);
		}
		function doInj() {
			// Note that this function operates on a frame element and not a
			// document because the document is probably not yet loaded.
			var el = this, doc = getContDoc(el);

			try {
				if (doc.hasInj) return; // Only inject the script once
				doc.hasInj = true;
			}
			catch(e) { return; } // domain errors

			if (!hasFCL) {
				var win = getWin(doc);
				win.onunload = function() { startDCLSpam(el, doc); };
			}

			var frel = doc.getElementsByTagName('frame');
			if (frel.length !== 0) {
				if (hasFCL) return;
				for (var i = 0; i < frel.length; ++i) {
					setInjFr(frel[i]);
				}
			}
			else {
				injCont(doc);
			}
		}
		function frContLoaded(e) {
			if (e && e.target) {
				hasFCL = true;
				try {
					if (e.target.nodeName === 'FRAME') doInj.call(e.target);
				}
				catch(e) {} // domain errors
			}
		}

		// Recursively inject the script and the script injection for the
		// second frame.
		var frs = window.document.getElementsByTagName('frame');
		setInjFr(frs[0]);
		setInjFr(frs[1]);

		// Also try using the Mozilla-specific DOMFrameContentLoaded event
		// (sibling to DOMContentLoaded) to make the effects appear more
		// instantly, without having to use the onunload hack. This isn't
		// cross-browser though.
		try {
			window.document.addEventListener('DOMFrameContentLoaded', frContLoaded, false);
		} catch(e) {}
	}


	// The actual content script, run on every subpage.
	function injCont(d) {
		var helpPage = !!d.helpPage;
		var path = (helpPage ? '#help-page' : getPath(d.URL));
		var mainPage = (path === topPath + "/personal/index.phtml");
		var headerPage = (path === topPath + "/personalframe.phtml");
		var loadingPage = (path === topPath + "/waiting.php");
		var roomPage = (d.URL.indexOf("/adm/projects.phtml?mode=displayRoomchooser") !== -1);

		try {
			var w = getWin(d);
			w.onfocus = tabFocused;
		}
		catch(e) {}

		// Use injHeader for injecting the header, instead of this function.
		if (headerPage) {
			injHeader(d);
			return;
		}

		if (loadingPage)
			return;

		// Add some styling.
		addStylesheetRules(d, {
			// General rules
			'body': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'td': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'div': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'input': 'font-family: "lucida grande",tahoma,verdana,arial,sans-serif !important',
			'a': 'white-space: normal !important',
			'[nowrap] a': 'white-space: nowrap !important',
			'::-webkit-input-placeholder': 'color: #777 !important',
			':-moz-placeholder': 'color: #777 !important',
			'.hasPlaceholder': 'color: #777 !important',

			// Basic styling
			'html': 'border-top: 1px solid #ccc',
			'body.personal': 'margin-top: 9px',
			'.customise-today-class-lib': 'table-layout: fixed',
			'.main-content-container': [
				'padding-right: 152px',
				'max-width: 1133px',
				'margin: 0 auto'
			],

			// Reminder list
			'.reminder-list': [
				'list-style-type: none',
				'margin: 0',
				'padding: 0'
			],
			'.reminder-list > li': [
				'border-bottom: 1px dotted #E0E0E0',
				'padding: 4px 0'
			],

			// Sidebar
			'.nav-header': [
				'font-size: 9px',
				'font-weight: bold',
				'color: #999',
				'margin-bottom: 5px',
				'width: 160px'
			],
			'.nav-list': [
				'font-size: 11px',
				'list-style-type: none',
				'margin: 0',
				'padding: 0'
			],
			'.nav-list > li': [
				'position: relative',
				'margin: 1px 0'
			],
			'.nav-list > li:hover': [
				'background-color: #eff2f7'
			],
			'.nav-link': [
				'display: block',
				'padding: 2px 0 2px 34px',
				'color: #333',
				'text-decoration: none !important'
			],
			'.nav-selected': [
				'background-color: #d8dfea',
				'font-weight: bold'
			],
			'.nav-list > .nav-selected:hover': [
				'background-color: #d8dfea'
			],
			'i.icon': [
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-navicons.png")',
				'background-repeat: no-repeat',
				'position: absolute',
				'left: 12px',
				'height: 16px',
				'width: 16px',
				'display: block'
			],
			'li.nav-home i.icon': [
				'background-position: -1px -333px'
			],
			'li.nav-arkiv i.icon': [
				'background-position: -20px -288px'
			],
			'li.nav-elevinfo i.icon' : [
				'background-position: -16px -155px'
			],
			'li.nav-absence i.icon' : [
				'background-position: -16px -172px'
			],
			'li.nav-sched i.icon' : [
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-sprite1.png")',
				'background-position: -18px -136px',
				'height: 17px'
			],
			'li.nav-games i.icon' : [
				'background-position: -17px -401px'
			],

			// News item rules.
			'div.newsitem > img': [
				'position: absolute',
				'width: 50px',
				'clip: rect(1px 50px 51px 0px)',
				'margin-top: -1px'
			],
			'div.newsiteminfo': [
				'margin-left: 55px',
				'font-size: 11px'
			],
			'.news-contents': [
				'margin-top: 1px !important',
				'font-size: 11px',
				'overflow: auto'
			],
			'hr.news-separator': [
				'background-color: #E9E9E9'
			],
			'a.main-name': [
				'color: #3B5998',
				'font-weight: bold'
			],
			'a.fakelink': [
				'color: #3B5998',
				'cursor: pointer'
			],
			'i.onlyclass': [
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-boxes.png")',
				'background-position: -273px -531px',
				'height: 10px',
				'width: 10px',
				'position: relative',
				'display: inline-block',
				'left: 0'
			],
			'.faded': 'color: #999',
			'.faded > a': 'color: #6D84B4',
			'a.morebtn': [
				'cursor: pointer'
			],
			'ul.actions': [
				'list-style-type: none',
				'font-size: 11px',
				'margin: 0',
				'padding: 0',
				'color: #333'
			],
			'ul.actions > li': [
				'display: block'
			],
			'li.acBorder': [
				'border-width: 0',
				'margin-bottom: -2px'
			],
			'li.acBorder > i': [
				'display: block',
				'background-image: url("http://simonsoftware.se/other/fronter/book/comment-arrow.png")',
				'height: 5px',
				'margin-left: 17px',
				'width: 9px',
				'background-repeat: no-repeat'
			],
			'li.acItem': [
				'display: block',
				'margin-top: 1px',
				'border-bottom: 1px solid #D2D9E7',
				'background-color: #EDEFF4',
				'padding: 5px 5px 4px'
			],
			'li.acItem > i': [
				'background-repeat: no-repeat',
				'position: absolute',
				'display: block'
			],
			'li.acLikes > i': [
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-sprite1.png")',
				'background-position: -14px -156px',
				'height: 13px',
				'width: 15px'
			],
			'li.acLikes > span': [
				'padding-left: 18px'
			],
			'li.acLikes > span > a': [
				'cursor: pointer',
				'color: #3B5998'
			],
			'li.acViewAll > i': [
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-sprite1.png")',
				'background-position: -173px -118px',
				'height: 16px',
				'width: 16px',
				'margin-top: -1px'
			],
			'li.acViewAll > a': [
				'position: relative',
				'color: #3B5998',
				'cursor: pointer',
				'padding-left: 18px'
			],
			'li.acComments > ul': [
				'list-style-type: none',
				'margin: 0',
				'padding: 0'
			],
			'li.acItem img': [
				'position: absolute',
				'width: 32px',
				'clip: rect(1px 32px 33px 0px)',
				'margin-top: -1px'
			],
			'.acComment': [
				'min-height: 32px'
			],
			'.acComment-contents': [
				'margin-left: 37px',
				'padding-right: 20px',
				'font-size: 11px'
			],
			'.acComment-info': [
				'margin-top: 3px',
				'color: #808080'
			],
			'li.acWrite': 'display: block',
			'.input-wrapper > input': [
				'width: 100%',
				'font-size: 11px',
				'margin: 0 !important',
				'border: 0 none',
				'-webkit-appearance: none',
				'color: #1A1A1A'
			],
			'.input-wrapper': [
				'display: block',
				'padding: 2px 3px',
				'border: 1px solid #BDC7D8',
				'background-color: white'
			],

			// Likes
			'.liketext': [
				'display: none',
				'color: #3B5998'
			],
			'.liketext > i': [
				'display: inline-block',
				'height: 9px',
				'width: 10px',
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-boxes.png")',
				'background-position: -110px -511px',
				'background-repeat: no-repeat',
				'margin-right: 4px'
			],
			'.liketext > span': [
				'cursor: pointer',
				'margin-left: -17px',
				'padding: 0 1px 0 17px'
			],

			// Status update
			'#status-header': [
				'position: relative',
				'height: 23px',
				'margin-top: 1px'
			],
			'#status-header > i.icon': [
				'background-position: -20px -288px',
				'left: 2px',
				'margin-top: -1px'
			],
			'#status-header > span': [
				'font-weight: bold',
				'font-size: 11px',
				'color: #333',
				'margin-left: 21px'
			],
			'#status-header > span > i.icon': [
				'position: absolute',
				'display: block',
				'background-image: url("http://simonsoftware.se/other/fronter/book/entry-arrow.gif")',
				'height: 6px',
				'width: 9px',
				'left: 5px',
				'top: 18px'
			],
			'#status-block': [
				'border: 1px solid #BDC7D8',
				'margin-bottom: 15px'
			],
			'#status-tawrapper': [
				'padding: 5px',
				'height: 16px',
				'color: #1a1a1a'
			],
			'#status-tawrapper textarea': [
				'border: 0px none',
				'-webkit-appearance: none',
				'width: 100%',
				'height: 100%',
				'margin: 0px',
				'font-family: "lucida grande",tahoma,verdana,arial,sans-serif',
				'resize: none',
				'overflow: hidden'
			],
			'#status-footer': [
				'display: none',
				'overflow: hidden',
				'background-color: #F2F2F2',
				'border-top: 1px solid #E6E6E6',
				'padding: 4px 4px 2px 0'
			],
			'#status-share': [
				'float: right',
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-boxes.png")',
				'background-repeat: no-repeat',
				'background-position: right -344px',
				'padding: 2px 23px 2px 6px',
				'margin: 1px 5px 0 0',
				'max-width: 169px',
				'cursor: pointer'
			],
			'#status-share-text': [
				'display: inline-block',
				'vertical-align: top',
				'padding-top: 1px',
				'font-size: 11px',
				'color: gray'
			],
			'#status-share > i.icon': [
				'position: static',
				'display: inline-block',
				'margin-right: 5px',
				'background-position: -17px -53px',
				'opacity: 0.5'
			],
			'#status-share:hover': [
				'background-position: right -49px',
				'margin: 0 4px 0 0',
				'border: 1px solid #999',
				'border-bottom-color: #888',
				'box-shadow: 0 1px 0 rgba(0, 0, 0, 0.1)'
			],
			'#status-share:hover > i.icon': [
				'opacity: 1'
			],
			'#status-share:hover > #status-share-text': [
				'color: #333'
			],
			'#status-footer > button': [
				'float: right',
				'font-family: "lucida grande",tahoma,verdana,arial,sans-serif',
				'font-size: 11px',
				'font-weight: bold',
				'padding: 3px 15px 4px',
				'background-color: #5B74A8',
				'background-image: url("http://simonsoftware.se/other/fronter/book/fb-boxes.png")',
				'background-position: 0 -98px',
				'background-repeat: no-repeat',
				'border: 1px solid #29447E',
				'border-bottom-color: #1A356E',
				'box-shadow: 0 1px 0 rgba(0, 0, 0, 0.1)',
				'color: white',
				'cursor: pointer'
			],
			'#status-footer > button:active': [
				'background: none repeat scroll 0 0 #4F6AA3',
				'border-bottom-color: #29447E',
				'outline: none'
			],
			'#status-footer > button::-moz-focus-inner': [
				'border: none'
			],

			// Chat sidebar
			'#chat-sidebar': [
				'position: fixed',
				'right: 0',
				'top: 0',
				'height: 100%',
				'width: 170px',
				'background-color: #EAEAEE',
				'border-left: 1px solid rgb(100, 100, 100)',
				'border-left: 1px solid rgba(0, 0, 0, 0.4)',
				'box-shadow: 2px 0 2px -2px #B2B9C9 inset',
				'overflow: hidden'
			],
			'#chat-sidebar-error': [
				'display: block',
				'margin: 4px 6px',
				'color: #222'
			],
			'#chat-sidebar-scroller': [
				'width: 150%',
				'height: 100%',
				'overflow-y: auto'
			],
			'#chat-sidebar-contents': [
				'padding: 8px 0',
				'width: 170px'
			],
			'#chat-sidebar-list': [
				'list-style-type: none',
				'margin: 0',
				'padding: 0',
				'font-size: 11px'
			],
			'#chat-sidebar-list > li': [
				'width: 100%'
			],
			'#chat-sidebar-list > li:hover': [
				'background-color: #DADEE8'
			],
			'#chat-sidebar-list div.avatar': [
				'background-color: #FFE4E3',
				'float: left',
				'width: 28px',
				'height: 28px',
				'margin-right: 7px'
			],
			'#chat-sidebar-list div.avatar > img': [
				'position: absolute',
				'width: 28px',
				'clip: rect(1px 28px 29px 0px)',
				'opacity: 0.9',
				'margin-top: -1px'
			],
			'#chat-sidebar-list a': [
				'position: relative',
				'display: block',
				'cursor: pointer',
				'text-decoration: none',
				'padding: 2px 1px 2px 10px'
			],
			'#chat-sidebar-list span.name': [
				'display: block',
				'line-height: 28px',
				'color: #333',
				'overflow: hidden',
				'white-space: nowrap',
				'text-overflow: ellipsis'
			],

			// Chats
			'#chats': [
				'display: none',
				'position: fixed',
				'z-index: 1000',
				'bottom: 0',
				'right: 170px'
			],
			'.chat': [
				'float: right',
				'height: 285px',
				'width: 260px',
				'margin-right: 12px',
				'margin-left: -1px',
				'box-shadow: 0 1px 1px rgba(0, 0, 0, 0.3)',
				'position: relative'
			],
			'.chat-title': [
				'border: 1px solid rgb(0, 91, 153)',
				'border: 1px solid rgba(0, 39, 121, 0.76)',
				'border-radius: 1px 1px 0 0',
				'border-bottom-style: none',
				'background-color: #4D68A2', //#6D84B4 unfocused
				'line-height: 18px',
				'height: 18px',
				'font-weight: bold',
				'font-size: 11px',
				'color: white',
				'padding: 3px 7px 4px',
				'overflow: hidden',
				'text-overflow: ellipsis',
				'white-space: nowrap'
			],
			'.chat-title-close': [
				'background-image: url(http://simonsoftware.se/other/fronter/book/fb-sprite3.png)',
				'background-position: -45px -217px',
				'background-repeat: no-repeat',
				'cursor: pointer',
				'position: absolute',
				'right: 3px',
				'top: 1px',
				'width: 21px',
				'height: 25px',
				'display: block',
				'text-decoration: none'
			],
			'.chat-title-close:hover': [
				'background-position: -23px -217px'
			],
			'.chat-content': [
				'border: 1px solid rgb(179, 179, 179)',
				'border: 1px solid rgba(0, 0, 0, 0.3)',
				'border-width: 0 1px',
				'background-color: white',
				'position: absolute',
				'overflow-y: auto',
				'bottom: 24px',
				'top: 26px',
				'left: 0',
				'right: 0'
			],
			'.chat-field': [
				'position: absolute',
				'bottom: 0',
				'height: 24px',
				'left: 0',
				'right: 0',
				'border: 1px solid rgb(179, 179, 179)',
				'border: 1px none rgba(0, 0, 0, 0.3)',
				'border-style: none solid',
				'background-color: white'
			],
			'.chat-field > i': [
				'display: block',
				'position: absolute',
				'left: 4px',
				'top: 5px',
				'background-image: url(http://simonsoftware.se/other/fronter/book/fb-sprite2.png)',
				'background-position: -81px -80px',
				'background-repeat: no-repeat',
				'height: 14px',
				'width: 14px'
			],
			'.chat-field > .input-wrapper': [
				'padding: 5px 4px 3px 20px',
				'border: 1px none #C9D0DA',
				'border-top-style: solid',
				'height: 20px'
			],
			'.chat-line': [
				'border-top: 1px solid #EEEEEE',
				'padding: 5px',
				'min-height: 32px'
			],
			'.chat-line > div': [
				'margin-left: 40px',
				'font-size: 11px'
			],
			'.chat-line > div > p': [
				'margin: 0 0 2px'
			],
			'.chat-line > img': [
				'position: absolute',
				'width: 32px',
				'clip: rect(1px 32px 33px 0px)',
				'margin-top: -1px'
			]
		});

		if (roomPage) {
			try {
				var tr = d.getElementsByTagName('tr')[0];
				tr.style.display = 'none';
				var t = getElementsByClassName(d, 'datatable')[0];
				var titleH = getElementsByClassName(d, 'label2')[1];
				setText(titleH, "Grupp");
				addStylesheetRules(d, {
					'.datatable th': 'border-top-width: 0',
					'.datatable td:last-of-type': 'display: none',
					'table.content-frame-content': 'display: none'
				});
			}
			catch(e) {
				console.log("Unexpected DOM structure.", e);
			}
		}

		function transformNews(newsMap) {
			var cls = ['news-odd', 'news-even'];
			for (var i = 0; i < 2; ++i) {
				var els = getElementsByClassName(d, cls[i]);
				for (var j = 0; j < els.length; ++j) {
					var news = News.convertExisting(d, els[j]);
					if (news) {
						newsMap[news.id] = news;
					}
					else {
						console.log("Failed to parse news.");
					}
				}
			}
		}

		function transformReminders() {
			var rem = getElementsByClassName(d, 'student-notification-element')[0];
			rem = rem && getElementsByClassName(rem, 'student-notification-element', 'table')[0];
			if (!rem) {
				console.log("No reminder element.");
				return;
			}
			var li = [];
			for (var i = 0; i < rem.rows.length; ++i) {
				var row = rem.rows[i];
				if (row.className !== 'tablelist-odd' && row.className !== 'tablelist-even')
					continue;
				var cols = row.cells;
				var title = cols[0].innerHTML,
					room = cols[1].innerHTML,
					left = cols[4].innerHTML;
				var nrow = title + "<br>" + room + "<br>" + left;
				li.push("<li>" + nrow + "</li>");
			}
			var html = "<ul class='reminder-list'>" + li.join('') + "</ul>";
			rem.parentNode.innerHTML = html;
		}

		function transformSidebar() {
			var sidebar = getElementsByClassName(d, 'customise-today infobox-display')[0];
			if (!sidebar) {
				console.log("No sidebar.");
				return;
			}
			var homeUrl = topPath + "/personal/index.phtml";
			var elevinfoUrl = topPath + "/contentframeset.phtml?navbar_size=&goto_prjid=362433948";
			var absenceUrl = topPath + "/absence2/";
			var schedUrl = "http://simonsoftware.se/s/";
			var frontrisUrl = topPath + "/games/";
			sidebar.innerHTML =
				"<div class='nav-header'>FAVORITER</div>" +
				"<ul class='nav-list'>" +
					"<li class='nav-home nav-selected'>" +
						"<a class='nav-link' href='" + homeUrl + "'>" +
							"<i class='icon'></i>Nyheter" +
						"</a>" +
					"</li>" +
					"<li class='nav-elevinfo'>" +
						"<a class='nav-link' href='" + elevinfoUrl + "'>" +
							"<i class='icon'></i>Elevinfo" +
						"</a>" +
					"</li>" +
					"<li class='nav-absence'>" +
						"<a class='nav-link' href='" + absenceUrl + "'>" +
							"<i class='icon'></i>Frånvaro" +
						"</a>" +
					"</li>" +
					"<li class='nav-sched'>" +
						"<a class='nav-link' href='" + schedUrl + "' target='_blank'>" +
							"<i class='icon'></i>Schema" +
						"</a>" +
					"</li>" +
					"<li class='nav-arkiv'>" +
						"<a class='nav-link' href='" + arkivUrl + "'>" +
							"<i class='icon'></i>Mitt arkiv" +
						"</a>" +
					"</li>" +
					"<li class='nav-games'>" +
						"<a class='nav-link' href='" + frontrisUrl + "'>" +
							"<i class='icon'></i>Frontris" +
						"</a>" +
					"</li>" +
				"</ul>";
		}

		function addStatusUpdate() {
			if (!d.newsContainer) {
				// The document doesn't contain news...
				return null;
			}

			var updateCont = d.createElement('div');
			updateCont.innerHTML =
				"<div id='status-header'>" +
					"<i class='icon'></i>" +
					"<span>" +
						"Nyheter" +
						"<i class='icon'></i>" +
					"</span>" +
				"</div>" +
				"<div id='status-block'>" +
					"<form id='status-form' action='' method='get'>" +
						"<div id='status-tawrapper'>" +
							"<textarea maxlength='900' placeholder='Skriv något...'></textarea>" +
						"</div>" +
						"<div id='status-footer'>" +
							"<button type='submit'>Skicka</button>" +
							"<a id='status-share' href='#'>" +
								"<i class='icon'></i>" +
								"<span id='status-share-text'></span>" +
							"</a>" +
						"</div>" +
					"</form>" +
				"</div>" +
				"<hr class='news-separator'>";

			var cont = d.newsContainer;
			while (cont.nodeName !== 'DIV') {
				cont = cont.parentNode;
			}
			cont.insertBefore(updateCont, cont.firstChild);

			// Space the status field away from the sidebar.
			cont.style.paddingLeft = '5px';

			var taWrapper = d.getElementById('status-tawrapper');
			var header = d.getElementById('status-header');
			var headerText = header.lastChild;
			var footer = d.getElementById('status-footer');
			var area = taWrapper.firstChild;
			headerText.onclick = area.onclick = function() {
				// Expand the area, and set up the wrapper to follow the size
				// of the input.
				function expand() {
					// This only vaguely resembles correctness in IE, but whatever.
					taWrapper.style.height = '48px';
					taWrapper.style.height = Math.max(area.scrollHeight, 48) + 'px';
				}
				if (area.attachEvent) {
					area.attachEvent('onkeypress', function() {
						setTimeout(expand, 0);
					});
				}
				else if (area.addEventListener) {
					area.addEventListener('input', expand, false);
				}

				function show() {
					if (footer.style.display !== 'block') {
						shareToAll = true;
						footer.style.display = 'block';
						expand();
						updateShare();
					}
					area.focus();
				}
				show();
				headerText.onclick = area.onclick = show;
			};

			addPlaceholder(area);

			var shareToAll = true;
			var shareBtn = d.getElementById('status-share');
			function updateShare() {
				setText(shareBtn.lastChild, shareToAll ? "Skolan" : "Klassen");
			}
			shareBtn.onclick = function() {
				shareToAll = !shareToAll;
				updateShare();
				return false;
			};

			var form = d.getElementById('status-form');
			form.onsubmit = function() {
				var v = trim(area.value);
				if (v && area.className !== 'hasPlaceholder') {
					if (limitSpam(0, 60))
						return false;

					sendEntry(v, shareToAll);

					taWrapper.style.height = '';
					footer.style.display = 'none';
					area.value = '';

					// Update the placeholder.
					area.focus();
					area.blur();
				}
				return false;
			};
		}

		if (mainPage) {
			hideName(d);
			transformSidebar();
			transformReminders();
			var chatSidebar = new ChatSidebar(d);

			var newsMap = {}, entryMap = {};
			d.likeObjects = {};
			d.unusedLikes = {};
			transformNews(newsMap);
			getFirstEvents(function() {
				if (!d.newsContainer)
					return;
				var commentsMap = {};
				var entries = [], likes = [];
				for (var i = 0; i < allEvents.length; ++i) {
					var ev = allEvents[i];
					if (ev.type === 0) { // comment
						if (!commentsMap[ev.parent])
							commentsMap[ev.parent] = [];
						commentsMap[ev.parent].push(ev);
						var entry = entryMap[ev.parent];
						if (entry)
							++entry.nComments;
					}
					else if (ev.type === 1) { // entry
						ev.nLikes = ev.nComments = 0;
						entries.push(ev);
						entryMap[ev.id] = ev;
					}
					else if (ev.type === 2) { // like
						likes.push(ev);
						var entry = entryMap[ev.parent];
						if (entry)
							++entry.nLikes;
					}
				}

				entries.sort(compareEntries);

				var before = d.newsContainer.firstChild;
				if (before)
					before = before.nextSibling;
				for (var i = 0; i < entries.length; ++i) {
					var e = entries[i];
					newsMap[e.id] = News.addBefore(d, before,
							e.id, e.user, e.text, e.timestamp, e.toAll);
					if (before)
						before = before.nextSibling;
				}

				for (var newsId in commentsMap) {
					if (!commentsMap.hasOwnProperty(newsId)) continue;
					var comments = commentsMap[newsId];
					var news = newsMap[newsId];
					if (news) {
						news.addInitialComments(comments);
					}
				}

				for (var i = 0; i < likes.length; ++i) {
					var ev = likes[i];
					updateLike(d, ev);
				}

				addStatusUpdate();
			});
			getIncrementalEvents(function(from) {
				if (!d.newsContainer)
					return;
				for (var i = from; i < allEvents.length; ++i) {
					var ev = allEvents[i];
					if (ev.type === 0) { // comment
						var news = newsMap[ev.parent];
						if (news) {
							news.addComment(ev, false);
						}
					}
					else if (ev.type === 1) { // entry
						newsMap[ev.id] = News.addBefore(d, d.newsContainer.firstChild,
							ev.id, ev.user, ev.text, ev.timestamp, ev.toAll);
					}
					else if (ev.type === 2) { // like
						updateLike(d, ev);
					}
				}
			});

			if (d.querySelector) {
				var newMeetingBtn = d.querySelector('.calendar-element .action-link');
				if (newMeetingBtn) {
					setText(newMeetingBtn, "Skapa evenemang");
				}
			}

			// Change the footer slightly.
			var rssFeedLink = d.getElementById('todaypage-rss-feed-link');
			if (rssFeedLink) {
				rssFeedLink.style.display = 'none';
				var copyrightFooter = rssFeedLink.parentNode.getElementsByTagName('span')[0];
				if (copyrightFooter)
					setText(copyrightFooter, "© Fronter 2012-04-01");
			}

			// Make the layout 70-30 instead of 50-50.
			var container = getElementsByClassName(d, 'main-content-container', 'table')[0];
			if (container) {
				container.rows[0].cells[0].width = '70%';
				container.rows[0].cells[1].width = '30%';
				container.removeAttribute('width');

				// Hack for making the fixed-width layout work in Chrome
				if (window.innerWidth && window.innerWidth > 1195) {
					container.style.width = '1131px';
				}
			}
		}
		else {
			destroyMainPage();
		}
	}

	// Now start the script.
	injTop();
}

})();
