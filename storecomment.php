<?php
$pointerid = $_GET['id'];
$ownerid = $_GET['user'];
$text = $_GET['text'];

// Type 0 for comments. Note: bind_param takes references, so we can't use
// a literal instead.
$type = 0;

// Make sure the comment is <= 100 characters.
if(strlen($text) > 100)
{
	echo('throw new Error("Comment too long");');
	exit();
}

// Store it in the database.
$mysqli = new mysqli('localhost', 'root', 'password', 'fronterbook') or die('throw new Error("Could not connect to database");'); 
if($statement = $mysqli->prepare('INSERT INTO fronterbook_event (ownerid, type, text, pointerid) VALUES (?, ?, ?, ?)')) {
	$statement->bind_param('iisi', $ownerid, $type, $text, $pointerid);
	$statement->execute();
	$statement->close();
}
else {
	echo 'throw new Error("Could not put post in table.");';
}
$mysqli->close();
 
// If everything works out, give a periodic update back.
include('getevents.php');
?>
